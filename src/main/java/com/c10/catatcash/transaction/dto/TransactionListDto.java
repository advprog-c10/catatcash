package com.c10.catatcash.transaction.dto;

import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.model.TransactionType;
import lombok.Data;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class TransactionListDto {
    
    private Long id;

    private String walletName;

    private String budgetName;

    private String description;

    private Long amount;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;

    private TransactionType type;

    public TransactionListDto(Transaction transaction) {
      this.id = transaction.getId();
      this.walletName = transaction.getWallet().getName();
      this.budgetName = (transaction.getBudget() == null) ? "-" : transaction.getBudget().getName();
      this.description = transaction.getDescription();
      this.amount = transaction.getAmount();
      this.date = transaction.getDate();
      this.type = transaction.getType();
    }
}