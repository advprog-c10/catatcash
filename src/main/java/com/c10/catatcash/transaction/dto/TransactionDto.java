package com.c10.catatcash.transaction.dto;

import com.c10.catatcash.budget.model.Budget;
import com.c10.catatcash.transaction.model.TransactionType;
import com.c10.catatcash.wallet.model.Wallet;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@NoArgsConstructor
public class TransactionDto {
    
    private Wallet wallet;

    private Budget budget;

    private String description;

    private Long amount;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;

    private TransactionType type;
}