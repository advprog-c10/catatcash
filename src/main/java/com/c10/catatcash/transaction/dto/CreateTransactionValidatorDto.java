package com.c10.catatcash.transaction.dto;

import com.c10.catatcash.transaction.model.Transaction;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CreateTransactionValidatorDto {
  
  private Boolean isValid;

  private Transaction transaction;

  public CreateTransactionValidatorDto(Boolean isValid, Transaction transaction) {
    this.isValid = isValid;
    this.transaction = transaction;
  }
}