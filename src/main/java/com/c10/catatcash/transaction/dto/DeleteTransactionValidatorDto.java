package com.c10.catatcash.transaction.dto;

import com.c10.catatcash.transaction.model.Transaction;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DeleteTransactionValidatorDto {
  
  private Boolean isValid;

  private List<Transaction> transactionList;

  public DeleteTransactionValidatorDto(Boolean isValid, List<Transaction> transactionList) {
    this.isValid = isValid;
    this.transactionList = transactionList;
  }
}