package com.c10.catatcash.transaction.initializer;

import com.c10.catatcash.transaction.core.FilterStrategy;
import com.c10.catatcash.transaction.core.FilterByBudget;
import com.c10.catatcash.transaction.core.FilterByNoFilter;
import com.c10.catatcash.transaction.core.FilterByTimestampInterval;
import com.c10.catatcash.transaction.core.FilterByType;
import com.c10.catatcash.transaction.core.FilterByWallet;
import com.c10.catatcash.transaction.repository.FilterStrategyRepository;
import com.c10.catatcash.transaction.repository.TransactionRepository;
import com.c10.catatcash.wallet.repository.WalletRepository;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Initializer untuk berbagai jenis filter.
 */
@Component
public class FilterStrategyInitializer {

  @Autowired
  private FilterStrategyRepository filterStrategyRepository;

  @Autowired
  private TransactionRepository transactionRepository;

  @Autowired
  private WalletRepository walletRepository;

  /**
   * Mengisi FilterStrategyRepository dengan berbagai filter.
   */
  @PostConstruct
  public void init() {
    List<FilterStrategy> filterStrategyList = Arrays.asList(
      new FilterByTimestampInterval(transactionRepository),
      new FilterByWallet(transactionRepository),
      new FilterByNoFilter(transactionRepository),
      new FilterByBudget(transactionRepository),
      new FilterByType(transactionRepository)
    );

    for (FilterStrategy filterStrategy : filterStrategyList) {
      this.filterStrategyRepository.addFilterStrategy(filterStrategy);
    }
  }
}