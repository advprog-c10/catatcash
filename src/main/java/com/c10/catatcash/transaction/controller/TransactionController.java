package com.c10.catatcash.transaction.controller;

import com.c10.catatcash.budget.service.BudgetService;
import com.c10.catatcash.transaction.dto.TransactionDto;
import com.c10.catatcash.transaction.dto.TransactionListDto;
import com.c10.catatcash.transaction.dto.CreateTransactionValidatorDto;
import com.c10.catatcash.transaction.dto.DeleteTransactionValidatorDto;
import com.c10.catatcash.transaction.service.TransactionService;
import com.c10.catatcash.transaction.service.TransactionValidatorService;
import com.c10.catatcash.wallet.service.WalletService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controller untuk transaction.
 */
@Controller
@RequestMapping(path = "/transaction")
public class TransactionController {
  
  @Autowired
  private TransactionService transactionService;

  @Autowired
  private WalletService walletService;

  @Autowired
  private BudgetService budgetService;

  @Autowired
  private TransactionValidatorService transactionValidatorService;

  private static final String REDIRECT_URL = "redirect:/transaction";
  private static final String FAIL_OPERATION_PAGE = "transaction/fail_operation";
  private static final String TRANSACTION_LIST_TICKET = "transactionListTicket";

  /**
   * Halaman utama dari transaksi.
   * Secara default akan menampilkan semua transaksi dari user.
   * Pilihan untuk melakukan penghapusan dan filter transaksi juga ada di halaman ini.
   */
  @GetMapping("")
  public String transactionHome(Model model) {
    boolean isFiltered = (model.asMap().get("isFiltered") != null);
    if (isFiltered) {
      model.addAttribute(TRANSACTION_LIST_TICKET, model.asMap().get(TRANSACTION_LIST_TICKET));
    } else {
      model.addAttribute(TRANSACTION_LIST_TICKET,
          transactionService.getTicketForGetListTransactionByFilter("by-nofilter", ""));
    }

    model.addAttribute("walletList", walletService.getWallets());
    model.addAttribute("budgetList", budgetService.getBudgetsByUser());

    return "transaction/transaction";
  }

  @PostMapping(path = "/getTransactionListFromTicket", produces = {"application/json"})
  @ResponseBody
  public ResponseEntity<Iterable<TransactionListDto>> getTransactionListFromTicket(@RequestParam("ticket") String ticket) {
    return ResponseEntity.ok(transactionService.getListTransactionFromTicket(ticket));
  }

  /**
   * Endpoint untuk melakukan filter.
   * Filter akan di-apply dan redirect ke /.
   */
  @GetMapping("/filter")
  public String changeFilter(
      @RequestParam("filterType") String filterType,
      @RequestParam(value = "filterParam", required = false) String filterParam,
      RedirectAttributes redirectAttributes) {
    
    redirectAttributes.addFlashAttribute(TRANSACTION_LIST_TICKET,
        transactionService.getTicketForGetListTransactionByFilter(filterType, filterParam));
    
    redirectAttributes.addFlashAttribute("isFiltered", true);
    
    return REDIRECT_URL;
  }

  /**
   * Halaman untuk membuat transaksi baru.
   */
  @GetMapping("/create") 
  public String createTransaction(Model model) {
    model.addAttribute("newTransaction", new TransactionDto());
    model.addAttribute("wallets", walletService.getWallets());
    model.addAttribute("budgets", budgetService.getBudgetsByUser());
    return "transaction/create";
  }

  /**
   * Menerima post request dari halaman /create dan redirect ke /.
   */
  @PostMapping("/create")
  public String createTransaction(@ModelAttribute TransactionDto newTransactionDto) {
    CreateTransactionValidatorDto validator = 
      transactionValidatorService.isValidCreateTransaction(newTransactionDto);

    if (!validator.getIsValid().booleanValue()) {
      return FAIL_OPERATION_PAGE;
    }

    transactionService.createTransaction(validator.getTransaction());
    
    return REDIRECT_URL;
  }

  /**
   * Menerima post request untuk menghapus transaksi.
   */
  @PostMapping("/delete")
  public String deleteTransactions(
      @RequestParam(value = "deletedTransactions", required = false)
      String deletedTransactions) {
    if (deletedTransactions != null) {
      String[] transactionIdString = deletedTransactions.split(",");
      List<Long> transactionIdLongList = new ArrayList<>();
      for (String id : transactionIdString) {
        if (!id.matches("\\d+")) {
          return FAIL_OPERATION_PAGE;
        }
        transactionIdLongList.add(Long.parseLong(id));
      }

      DeleteTransactionValidatorDto validator = transactionValidatorService.isValidDeleteTransactionById(transactionIdLongList);

      if (!validator.getIsValid().booleanValue()) {
        return FAIL_OPERATION_PAGE;
      }
      
      transactionService.deleteTransactions(validator.getTransactionList());
    }
    return REDIRECT_URL;
  }

  @PostMapping(path = "/validate/create", produces = {"application/json"})
  @ResponseBody
  public ResponseEntity<Map<String, Boolean>> validateCreateTransaction(@ModelAttribute TransactionDto newTransactionDto) {
    Map<String, Boolean> validationStatus = new HashMap<>();
    CreateTransactionValidatorDto validator = 
      transactionValidatorService.isValidCreateTransaction(newTransactionDto);
    validationStatus
        .put("isValidated", validator.getIsValid());
    return ResponseEntity.ok(validationStatus);
  }
}