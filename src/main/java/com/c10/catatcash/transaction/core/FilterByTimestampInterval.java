package com.c10.catatcash.transaction.core;

import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.repository.TransactionRepository;
import com.c10.catatcash.userauth.model.User;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Filter untuk mengembalikan semua transaksi seorang user pada suatu interval waktu tertentu.
 */
public class FilterByTimestampInterval implements FilterStrategy {

  private TransactionRepository transactionRepository;

  public FilterByTimestampInterval(TransactionRepository transactionRepository) {
    this.transactionRepository = transactionRepository;
  }

  public String getType() {
    return "by-timestampinterval";
  }

  public Iterable<Transaction> getFilteredTransaction(String param, User user) {

    try {
      Date startDate = getStartDate(param);
      Date endDate = getEndDate(param);
      return transactionRepository.findByDateGreaterThanEqualAndDateLessThanEqualAndWalletAppUserOrderByDateAsc(
          startDate, endDate, user);
    } catch (Exception e) {
      return null;
    }
  }

  private Date getStartDate(String param) throws ParseException {
    String parsedParam = parse(param)[0];
    return new SimpleDateFormat("yyyy-MM-dd").parse(parsedParam);
  }

  private Date getEndDate(String param) 
      throws ParseException, ArrayIndexOutOfBoundsException {
    String parsedParam = parse(param)[1];
    return new SimpleDateFormat("yyyy-MM-dd").parse(parsedParam);
  }

  private String[] parse(String param) {
    return param.split(",");
  }  
}