package com.c10.catatcash.transaction.core;

import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.repository.TransactionRepository;
import com.c10.catatcash.userauth.model.User;

/**
 * Filter untuk mengembalikan semua transaksi milik seorang user.
 */
public class FilterByNoFilter implements FilterStrategy {
  
  private TransactionRepository transactionRepository;

  public FilterByNoFilter(TransactionRepository transactionRepository) {
    this.transactionRepository = transactionRepository;
  }

  public String getType() {
    return "by-nofilter";
  }

  public Iterable<Transaction> getFilteredTransaction(String param, User user) {
    return transactionRepository.findByWalletAppUserOrderByDateAsc(user);
  }
}