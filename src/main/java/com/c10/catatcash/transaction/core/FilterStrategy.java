package com.c10.catatcash.transaction.core;

import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.userauth.model.User;

/**
 * Interface dari filter.
 */
public interface FilterStrategy {

  String getType();

  Iterable<Transaction> getFilteredTransaction(String param, User user);
}