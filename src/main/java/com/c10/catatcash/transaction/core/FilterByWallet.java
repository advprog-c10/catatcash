package com.c10.catatcash.transaction.core;

import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.transaction.repository.TransactionRepository;

/**
 * Filter untuk mengembalikan semua transaksi dari wallet yang dipilih dan dimiliki seorang user.
 */

public class FilterByWallet implements FilterStrategy {
  
  private TransactionRepository transactionRepository;

  public FilterByWallet(TransactionRepository transactionRepository) {
    this.transactionRepository = transactionRepository;
  }

  public String getType() {
    return "by-wallet";
  }

  public Iterable<Transaction> getFilteredTransaction(String param, User user) {
    try {
      Long walletId = Long.parseLong(param);
      return transactionRepository.findByWalletWalletIdAndWalletAppUserOrderByDateAsc(walletId, user);
    } catch (NumberFormatException e) {
      return null;
    }
  }
}