package com.c10.catatcash.transaction.core;

import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.model.TransactionType;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.transaction.repository.TransactionRepository;

/**
 * Filter untuk mengembalikan semua transaksi yang bertipe sesuai pilihan user (expense / income).
 */

public class FilterByType implements FilterStrategy {
  
  private TransactionRepository transactionRepository;

  public FilterByType(TransactionRepository transactionRepository) {
    this.transactionRepository = transactionRepository;
  }

  public String getType() {
    return "by-type";
  }

  public Iterable<Transaction> getFilteredTransaction(String param, User user) {
    if (param.equals(TransactionType.INCOME.getDisplayValue())) {
      return transactionRepository.findByTypeAndWalletAppUserOrderByDateAsc(TransactionType.INCOME, user);
    }
    return transactionRepository.findByTypeAndWalletAppUserOrderByDateAsc(TransactionType.EXPENSE, user);
  }
}