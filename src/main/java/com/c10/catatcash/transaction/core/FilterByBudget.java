package com.c10.catatcash.transaction.core;

import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.transaction.repository.TransactionRepository;

/**
 * Filter untuk mengembalikan semua transaksi dari budget yang dipilih user.
 */

public class FilterByBudget implements FilterStrategy {
  
  private TransactionRepository transactionRepository;

  public FilterByBudget(TransactionRepository transactionRepository) {
    this.transactionRepository = transactionRepository;
  }

  public String getType() {
    return "by-budget";
  }

  public Iterable<Transaction> getFilteredTransaction(String param, User user) {
    try {
      Long budgetId = Long.parseLong(param);
      return transactionRepository.findByBudgetIdAndWalletAppUserOrderByDateAsc(budgetId, user);
    } catch (NumberFormatException e) {
      return null;
    }
  }
}