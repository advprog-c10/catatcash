package com.c10.catatcash.transaction.model;

import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.budget.model.Budget;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Model untuk Transaction.
 */
@Entity
@NoArgsConstructor
@Data
@Table(name = "transaction")
public class Transaction {
    
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", nullable = false)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "wallet", nullable = false)
  private Wallet wallet;

  @ManyToOne
  @JoinColumn(name = "budget", nullable = true)
  private Budget budget;

  @Column(name = "description", nullable = false)
  private String description;

  @Column(name = "amount", nullable = false)
  private Long amount;

  @Column(name = "date", nullable = false)
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date date;
   
  @Column(name = "type", nullable = false)
  @Enumerated(EnumType.STRING)
  private TransactionType type;

  /**
   * Constructor.
   */
  public Transaction(Wallet wallet, Budget budget, String description, 
      Long amount, Date date, TransactionType type) {
            
    this.wallet = wallet;
    this.description = description;
    this.amount = amount;
    this.date = date;
    this.type = type;

    if (this.type == TransactionType.EXPENSE) {
      this.amount *= -1;
      this.budget = budget;
    }
  }
}