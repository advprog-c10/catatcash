package com.c10.catatcash.transaction.model;

public enum TransactionType {
  INCOME("Income"),
  EXPENSE("Expense");

  private final String displayValue;
    
  private TransactionType(String displayValue) {
      this.displayValue = displayValue;
  }
    
  public String getDisplayValue() {
    return displayValue;
  }
}