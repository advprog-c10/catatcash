package com.c10.catatcash.transaction.service;

import com.c10.catatcash.budget.model.Budget;
import com.c10.catatcash.budget.service.BudgetService;
import com.c10.catatcash.transaction.dto.TransactionDto;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.repository.TransactionRepository;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.service.WalletService;
import com.c10.catatcash.wallet.repository.WalletRepository;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.userauth.repository.UserRepository;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

@EnableAsync(proxyTargetClass = true)
@Service
public class TransactionUtilityServiceImpl implements TransactionUtilityService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private BudgetService budgetService;

  @Autowired
  private WalletService walletService;
  
  @Autowired
  private WalletRepository walletRepository;

  @Autowired
  private TransactionRepository transactionRepository;

  public Transaction dtoToTransaction(TransactionDto transactionDto) {
    return new Transaction(
        transactionDto.getWallet(),
        transactionDto.getBudget(),
        transactionDto.getDescription(),
        transactionDto.getAmount(),
        transactionDto.getDate(),
        transactionDto.getType()
        ); 
  }

  public boolean isEveryWalletBalanceGreaterOrEqualZero(Iterable<Long> walletBalance) {
    for (Long balance : walletBalance) {
      if (balance < 0) {
        return false;
      }
    }
    return true;
  }

  public Transaction isRequestMadeByWalletOwner(Transaction transaction) {
    for (Wallet wallet : walletService.getWallets()) {
      if (transaction.getWallet().getWalletId().equals(wallet.getWalletId())) {
        transaction.setWallet(wallet);
        return transaction;
      }
    }
    return null;
  }

  public User getCurrentUser() {
    String currentEmail = SecurityContextHolder.
                getContext().
                getAuthentication().
                getName();
    
    return userRepository.findByEmail(currentEmail);
  }

  public void setBalance(Wallet wallet, Transaction transaction, boolean isCreateTransaction) {
    Long amount = transaction.getAmount() * (isCreateTransaction ? 1 : -1);
    wallet.setBalance(wallet.getBalance() + amount);
    walletRepository.save(wallet);

    Budget budget = transaction.getBudget();
    if (budget != null) {
      budgetService.addSpend(-1 * amount, budget.getId());
    }
  }

  public List<Transaction> getTransactionListByIdIn(List<Long> transactionIdList) {
    Iterable<Transaction> transactionIterable = transactionRepository.findByIdIn(transactionIdList);
    List<Transaction> transactionList = new ArrayList<>();
    for (Transaction transaction : transactionIterable) {
      transactionList.add(transaction);
    }
    return transactionList;
  }

  public Map<Long, Long> getBalanceMap() {
    Iterable<Wallet> walletIterable = walletService.getWallets();
    HashMap<Long, Long> balanceMap = new HashMap<>();
    for (Wallet wallet : walletIterable) {
      balanceMap.put(wallet.getWalletId(), wallet.getBalance());
    }
    return balanceMap;
  }

  public String getTicketForTransactionList() {
    byte[] randomBytes = new byte[64];
    new SecureRandom().nextBytes(randomBytes);
    return javax.xml.bind.DatatypeConverter.printHexBinary(randomBytes);
  }

  /* untuk menghapus tiket setelah jeda waktu tertentu */
  @Async
  public void asyncDeleteFromTicketMap(Map<String, CompletableFuture<Iterable<Transaction>>> ticketMap, String ticket) {
    try {
      Thread.sleep(5000);
    } catch (Exception e) {
      Thread.currentThread().interrupt();
    }
    ticketMap.remove(ticket);
  }

  public void deleteBudgetFromTransactionByBudgetId(Long id){
    Iterable<Transaction> transactionList = transactionRepository.findByBudgetIdAndWalletAppUser(id, getCurrentUser());
    for (Transaction transaction : transactionList) {
      transaction.setBudget(null);
      transactionRepository.save(transaction);
    }
  }
}