package com.c10.catatcash.transaction.service;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.dto.TransactionListDto;
import com.c10.catatcash.userauth.model.User;

/**
 * Service untuk Transaction.
 */
public interface TransactionService {
  
  public void createTransaction(Transaction transaction);

  public void deleteTransactions(Iterable<Transaction> transactionList);

  public Iterable<Transaction> getListTransactionByFilter(
      String filterType, String filterParam, User user);

  public String getTicketForGetListTransactionByFilter(String filterType,
      String filterParam);
  
  public Iterable<TransactionListDto> getListTransactionFromTicket(String ticket);
}