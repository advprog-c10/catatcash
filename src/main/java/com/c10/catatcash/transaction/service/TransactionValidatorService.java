package com.c10.catatcash.transaction.service;

import com.c10.catatcash.transaction.dto.CreateTransactionValidatorDto;
import com.c10.catatcash.transaction.dto.DeleteTransactionValidatorDto;
import com.c10.catatcash.transaction.dto.TransactionDto;
import java.util.List;

public interface TransactionValidatorService {
  
  public CreateTransactionValidatorDto isValidCreateTransaction(TransactionDto transactionDto);

  public DeleteTransactionValidatorDto isValidDeleteTransactionById(List<Long> deletedTransactionIdList);
}