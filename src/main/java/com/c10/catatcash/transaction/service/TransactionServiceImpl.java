package com.c10.catatcash.transaction.service;

import com.c10.catatcash.budget.service.BudgetService;
import com.c10.catatcash.transaction.dto.TransactionListDto;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.repository.FilterStrategyRepository;
import com.c10.catatcash.transaction.repository.TransactionRepository;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.wallet.service.WalletService;
import java.util.concurrent.CompletableFuture;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementasi service untuk Transaction.
 */
@Service
public class TransactionServiceImpl implements TransactionService {

  @Autowired
  private TransactionRepository transactionRepository;

  @Autowired
  private FilterStrategyRepository filterStrategyRepository;

  @Autowired
  private BudgetService budgetService;

  @Autowired
  private WalletService walletService;

  @Autowired
  private TransactionUtilityService transactionUtilityService;

  private static final 
      Map<String, CompletableFuture<Iterable<Transaction>>> ticketMap = new HashMap<>();

  /**
   * Menyimpan transaksi ke repository dan mengupdate saldo wallet.
   */
  public void createTransaction(Transaction transaction) {
    transactionRepository.save(transaction);

    transactionUtilityService.setBalance(
        walletService.getWalletById(transaction.getWallet().getWalletId()),
        transaction, true
        );
  }

  /**
   * Menghapus transaksi dari repository dan mengupdate saldo wallet.
   */
  public void deleteTransactions(Iterable<Transaction> transactionList) {
    for (Transaction transaction : transactionList) {
      transactionRepository.delete(transaction);
      transactionUtilityService.setBalance(transaction.getWallet(), transaction, false);
    }
  }

  public Iterable<Transaction> getListTransactionByFilter(
      String filterType, String filterParam, User user) {
    return filterStrategyRepository.getFilteredTransaction(
        filterType, filterParam, user);
  }

  public String getTicketForGetListTransactionByFilter(String filterType,
    String filterParam) {
        String ticket = transactionUtilityService.getTicketForTransactionList();
        User user = transactionUtilityService.getCurrentUser();
        ticketMap.put(
            ticket,
            CompletableFuture.supplyAsync(() -> getListTransactionByFilter(filterType, filterParam, user))
        );
        transactionUtilityService.asyncDeleteFromTicketMap(ticketMap, ticket);

    return ticket;
  }

  public Iterable<TransactionListDto> getListTransactionFromTicket(String ticket) {
    CompletableFuture<Iterable<Transaction>> futureTransactionList = ticketMap.remove(ticket);
    List<TransactionListDto> transactionList = new ArrayList<>();
    try {
      for (Transaction transaction : futureTransactionList.get()) {
        transactionList.add(new TransactionListDto(transaction));
      }
    } catch (Exception e) {
      Thread.currentThread().interrupt();
    }
    return transactionList;
  }
}