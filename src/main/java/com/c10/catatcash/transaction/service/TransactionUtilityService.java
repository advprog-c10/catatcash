package com.c10.catatcash.transaction.service;

import com.c10.catatcash.transaction.dto.TransactionDto;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.wallet.model.Wallet;
import java.util.concurrent.CompletableFuture;
import java.util.List;
import java.util.Map;

public interface TransactionUtilityService {

  public Transaction dtoToTransaction(TransactionDto transactionDto);

  public boolean isEveryWalletBalanceGreaterOrEqualZero(Iterable<Long> walletBalance);

  public Transaction isRequestMadeByWalletOwner(Transaction transaction);
  
  public User getCurrentUser();

  public void setBalance(Wallet wallet, Transaction transaction, boolean isCreateTransaction);

  public List<Transaction> getTransactionListByIdIn(List<Long> transactionIdList);

  public Map<Long, Long> getBalanceMap();

  public String getTicketForTransactionList();

  public void asyncDeleteFromTicketMap(Map<String, CompletableFuture<Iterable<Transaction>>> ticketMap, String ticket);

  public void deleteBudgetFromTransactionByBudgetId(Long id);
  
}