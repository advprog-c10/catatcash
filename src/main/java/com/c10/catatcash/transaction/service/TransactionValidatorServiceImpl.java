package com.c10.catatcash.transaction.service;

import com.c10.catatcash.transaction.dto.TransactionDto;
import com.c10.catatcash.transaction.dto.CreateTransactionValidatorDto;
import com.c10.catatcash.transaction.dto.DeleteTransactionValidatorDto;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.repository.TransactionRepository;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.service.WalletService;
import com.c10.catatcash.wallet.repository.WalletRepository;
import com.c10.catatcash.userauth.repository.UserRepository;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionValidatorServiceImpl implements TransactionValidatorService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private WalletService walletService;
  
  @Autowired
  private WalletRepository walletRepository;

  @Autowired
  private TransactionRepository transactionRepository;

  @Autowired
  private TransactionUtilityService transactionUtilityService;

  public CreateTransactionValidatorDto isValidCreateTransaction(TransactionDto transactionDto) {
    Wallet wallet = walletRepository.findById(transactionDto.
        getWallet().
        getWalletId()).
        orElse(null);
    Transaction newTransaction = transactionUtilityService.
        dtoToTransaction(transactionDto);
    newTransaction = transactionUtilityService.
        isRequestMadeByWalletOwner(newTransaction);
    
    CreateTransactionValidatorDto validatorDto = new CreateTransactionValidatorDto(true, newTransaction);

    if (
        (newTransaction == null) ||
        (transactionDto.getAmount() <= 0) ||
        (wallet == null) ||
        (wallet.getBalance() + newTransaction.getAmount() < 0)
        ) {
      validatorDto.setIsValid(false);
    }

    return validatorDto;
  }

  public DeleteTransactionValidatorDto isValidDeleteTransactionById(List<Long> deletedTransactionIdList) {
    List<Transaction> deletedTransactionList = transactionUtilityService.getTransactionListByIdIn(deletedTransactionIdList);
    DeleteTransactionValidatorDto validatorDto = new DeleteTransactionValidatorDto(true, deletedTransactionList);
    Map<Long, Long> balanceMap = transactionUtilityService.getBalanceMap();

    boolean isEveryTransactionOwnedByRequestingUser = true;
    for (Transaction transaction : deletedTransactionList) {
      Long walletId = transaction.getWallet().getWalletId();
      if (!balanceMap.containsKey(walletId)) {
        isEveryTransactionOwnedByRequestingUser = false;
        break;
      }
      balanceMap.put(walletId, balanceMap.get(walletId) - transaction.getAmount());
    }

    if (!(
      (deletedTransactionList.size() == deletedTransactionIdList.size()) &&
      (transactionUtilityService.isEveryWalletBalanceGreaterOrEqualZero(balanceMap.values())) &&
      (isEveryTransactionOwnedByRequestingUser))
      ) {
      validatorDto.setIsValid(false);
    }

    return validatorDto;
  }
}