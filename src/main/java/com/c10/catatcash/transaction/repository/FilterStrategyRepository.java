package com.c10.catatcash.transaction.repository;

import com.c10.catatcash.transaction.core.FilterStrategy;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.userauth.model.User;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Repository;

/**
 * Repository untuk Filter.
 */
@Repository
public class FilterStrategyRepository {
    
  private Map<String, FilterStrategy> filterStrategies = new HashMap<>();

  /**
   * Menerapkan filter yang tepat yang diiinginkan user.
   */
  public Iterable<Transaction> getFilteredTransaction(
      String filterType, String filterParam, User user) {
        
    FilterStrategy filterStrategy = getFilterStrategyByType(filterType);
    if (filterStrategy == null) {
      return null;
    }

    return filterStrategy.getFilteredTransaction(filterParam, user);
  }

  /**
   * Mencari filter pada hashmap.
   */
  public FilterStrategy getFilterStrategyByType(String type) {
    if (filterStrategies.containsKey(type)) {
      return filterStrategies.get(type);
    }
    return null;
  }

  public void addFilterStrategy(FilterStrategy filterStrategy) {
    filterStrategies.put(filterStrategy.getType(), filterStrategy);
  }
}