package com.c10.catatcash.transaction.repository;

import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.model.TransactionType;
import com.c10.catatcash.userauth.model.User;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository untuk Transaction.
 */
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

  Iterable<Transaction> findByWalletAppUser(User user);

  Iterable<Transaction> findByDateGreaterThanEqualAndDateLessThanEqualAndWalletAppUser(Date startDate, Date endDate, User user);

  Iterable<Transaction> findByWalletWalletIdAndWalletAppUser(Long walletId, User user);

  Iterable<Transaction> findByIdIn(List<Long> transactionIdList);

  Iterable<Transaction> findByBudgetIdAndWalletAppUser(Long id, User user);

  Iterable<Transaction> findByWalletAppUserOrderByDateAsc(User user);

  Iterable<Transaction> findByDateGreaterThanEqualAndDateLessThanEqualAndWalletAppUserOrderByDateAsc(Date startDate, Date endDate, User user);

  Iterable<Transaction> findByWalletWalletIdAndWalletAppUserOrderByDateAsc(Long walletId, User user);

  Iterable<Transaction> findByBudgetIdAndWalletAppUserOrderByDateAsc(Long id, User user);

  Iterable<Transaction> findByTypeAndWalletAppUserOrderByDateAsc(TransactionType type, User user);
}