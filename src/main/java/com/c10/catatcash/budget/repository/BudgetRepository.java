package com.c10.catatcash.budget.repository;

import com.c10.catatcash.budget.model.Budget;
import com.c10.catatcash.userauth.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BudgetRepository  extends JpaRepository<Budget, Long>{
    Budget findByid(Long id);
    Iterable<Budget> findByWalletAppUser(User user);
}
