package com.c10.catatcash.budget.model;




import com.c10.catatcash.wallet.model.Wallet;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;


import javax.persistence.*;
import java.util.Date;



@Entity
    @Table(name="Budget")
    @Data
    @NoArgsConstructor

    public class Budget {
        @Id
        @Column(name = "id", updatable = false, nullable = false)
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Long id;

        @Column(name="name",nullable = false)
        private  String name;

        @Column(name = "amount",nullable = false)
        private  Long amount;


        @Column(name = "startTime")
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private  Date start;

        @Column(name = "endTime")
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private  Date end;

        @Column(name ="Spend")
        private Long spend;

        @ManyToOne
        @JoinColumn(name="wallet", nullable=false)
        private Wallet wallet;



        public Budget(Wallet wallet,String name,long amount ,Date start, Date end){
            this.wallet = wallet;
            this.name=name;
            this.amount=amount;
            this.start=start;
            this.end=end;
        }


    }