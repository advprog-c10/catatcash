package com.c10.catatcash.budget.dto;

import com.c10.catatcash.wallet.model.Wallet;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;


@Data
@NoArgsConstructor
public class BudgetDto {
    private Wallet wallet;

    private String name;

    private Long amount;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date start;

    @DateTimeFormat(pattern = "yyyy-MM-dd")

    private Date end;
}
