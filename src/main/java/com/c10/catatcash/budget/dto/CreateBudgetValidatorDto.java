package com.c10.catatcash.budget.dto;

import com.c10.catatcash.budget.model.Budget;
import  lombok.Data;
import  lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CreateBudgetValidatorDto {


    private Budget budget;

    public  CreateBudgetValidatorDto( Budget budget){

        this.budget = budget;
    }

}
