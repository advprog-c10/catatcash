package com.c10.catatcash.budget.controller;

import com.c10.catatcash.budget.dto.BudgetDto;
import com.c10.catatcash.budget.dto.CreateBudgetValidatorDto;
import com.c10.catatcash.budget.model.Budget;
import com.c10.catatcash.budget.service.BudgetDtoService;
import com.c10.catatcash.budget.service.BudgetService;
import com.c10.catatcash.wallet.exception.UnauthorizedAccessException;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(path = "/budget")
public class BudgetController {

    @Autowired
     BudgetService budgetService;

    @Autowired
    WalletService walletService;

    @Autowired
    BudgetDtoService budgetDtoService;

    private static final String REDIRECT_URL = "redirect:/wallet/add";

    private boolean checkWallet(){
        List<Wallet> wallets = (List<Wallet>) walletService.getWallets();
        return wallets.isEmpty();
    }
    private Budget cheackBudget(Long id){
       return budgetService.getBudgetByID(id);
    }

    @GetMapping("")
    public String budgetHome(Model model){

    model.addAttribute("budgets",budgetService.getBudgetsByUser());
        if(checkWallet()){
            return REDIRECT_URL;
        }

        return "budget/homeBudget";
    }

    @GetMapping(path = "/{budgetId}", produces = {"application/json"})
    public String budgetDetail(
            final Model model,
            final @PathVariable(value = "budgetId") Long budgetId){
        try {
            if(checkWallet()){
                return REDIRECT_URL;
            }
            else if(cheackBudget(budgetId)==null){
                return "redirect:/budget/add";
            }
            model.addAttribute("budget", budgetService.getBudgetByID(budgetId));
            return "budget/detail";
        } catch (UnauthorizedAccessException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return  "wallet/error_page";
        }
    }

    @DeleteMapping(path="/{budgetId}/delete")
    public String budgetDelete(
            final @PathVariable(value = "budgetId") Long budgetId,
            Model model){
        try {
            budgetService.deleteBudgetByID(budgetId);
            return "redirect:/budget";
        } catch (UnauthorizedAccessException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return  "wallet/error_page";
        }
    }




    @GetMapping("/add")
    public String addBudgetGet(Model model){
        List<Wallet> wallets = (List<Wallet>) walletService.getWallets();
        if(wallets.isEmpty()){
            return REDIRECT_URL;
        }

    model.addAttribute("newBudget", new BudgetDto());
    model.addAttribute("wallets", wallets);
        return "budget/budget_form";
    }

    @PostMapping("/add")
    public  String budgetSumbit(@ModelAttribute BudgetDto newBudgetDto){

        CreateBudgetValidatorDto validDto = budgetDtoService.isValidCreateBudget(newBudgetDto);

        budgetService.createBudget(validDto.getBudget());

        return "redirect:/budget";
    }


}
