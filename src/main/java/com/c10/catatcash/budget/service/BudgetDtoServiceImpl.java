package com.c10.catatcash.budget.service;


import com.c10.catatcash.budget.dto.BudgetDto;
import com.c10.catatcash.budget.dto.CreateBudgetValidatorDto;
import com.c10.catatcash.budget.model.Budget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BudgetDtoServiceImpl implements BudgetDtoService{


    @Autowired
    private BudgetUtilityService budgetUtilityService;

    @Override
    public CreateBudgetValidatorDto isValidCreateBudget(BudgetDto budgetDto) {

        Budget newBudget = budgetUtilityService.dtoToBudget(budgetDto);
        newBudget = budgetUtilityService.isRequestMadeByWalletOwner(newBudget);
        return new CreateBudgetValidatorDto(newBudget);
    }
}
