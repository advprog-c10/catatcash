package com.c10.catatcash.budget.service;

import com.c10.catatcash.budget.dto.BudgetDto;
import com.c10.catatcash.budget.model.Budget;
import com.c10.catatcash.budget.repository.BudgetRepository;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.userauth.repository.UserRepository;
import com.c10.catatcash.budget.exception.UnauthorizedAccessException;
import com.c10.catatcash.wallet.model.Wallet;


import com.c10.catatcash.wallet.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class BudgetUtilityServiceImpl implements BudgetUtilityService {

    @Autowired
    private WalletService walletService;

    @Autowired
    private UserRepository userRepository;


    @Autowired
    private BudgetRepository budgetRepository;

    @Override
    public Budget dtoToBudget(BudgetDto budgetDto) {
        return new Budget( budgetDto.getWallet(),
                budgetDto.getName(), budgetDto.getAmount(),
                budgetDto.getStart(),
                budgetDto.getEnd()
        );
    }

    @Override
    public Budget isRequestMadeByWalletOwner(Budget budget) {
        for (Wallet wallet: walletService.getWallets()){
            if (budget.getWallet().getWalletId().equals(wallet.getWalletId())){
                return budget;
            }
        }
        return null;
    }

    @Override
    public User getCurrentUser() {
        String currentEmail = SecurityContextHolder.
                getContext().
                getAuthentication().
                getName();
        return userRepository.findByEmail(currentEmail);
    }

    @Override
    public void validateUserOwnsBudget(Long budgetId) {
        Budget budget = budgetRepository.findByid(budgetId);
        if(budget==null){
            return;
        }
        Wallet wallet = budget.getWallet();
        if (wallet == null || !wallet.getAppUser().getId().equals(getCurrentUser().getId()) ) {
            throw new UnauthorizedAccessException("you don't have permission to access this budget");
        }
    }
}
