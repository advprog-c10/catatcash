package com.c10.catatcash.budget.service;

import com.c10.catatcash.budget.model.Budget;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.transaction.service.TransactionUtilityService;
import com.c10.catatcash.budget.repository.BudgetRepository;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;




@Service
public class BudgetServiceImpl implements BudgetService {

    @Autowired
    private BudgetRepository budgetRepository;

    @Autowired
    private BudgetUtilityService budgetUtilityService;

    @Autowired
    private TransactionUtilityService transactionUtilityService;

    @Override
    public Budget createBudget(Budget budget) {
        budget.setSpend(0L);
        budgetRepository.save(budget);
        return budget;
    }

    @Override
    public void deleteBudgetByID(Long id) {
        budgetUtilityService.validateUserOwnsBudget(id);
      transactionUtilityService.deleteBudgetFromTransactionByBudgetId(id);
      budgetRepository.deleteById(id);
    }

    @Override
    public Iterable<Budget> getBudgetsByUser() {
        User currentUser = budgetUtilityService.getCurrentUser();
        return budgetRepository.findByWalletAppUser(currentUser);
    }




    @Override
    public Budget getBudgetByID(Long id) {
        budgetUtilityService.validateUserOwnsBudget(id);
        return budgetRepository.findByid(id);
    }

    @Override
    public void addSpend(Long spend, Long id){
         Budget budget = budgetRepository.findByid(id);
         Long newspend = budget.getSpend() + spend;
         budget.setSpend(newspend);
         budgetRepository.save(budget);
    }


}
