package com.c10.catatcash.budget.service;


import com.c10.catatcash.budget.dto.BudgetDto;
import com.c10.catatcash.budget.model.Budget;
import com.c10.catatcash.userauth.model.User;


public interface BudgetUtilityService {

    Budget dtoToBudget(BudgetDto budgetDto);

    Budget isRequestMadeByWalletOwner(Budget budget);

    User getCurrentUser();

    void validateUserOwnsBudget(Long budgetId);
}
