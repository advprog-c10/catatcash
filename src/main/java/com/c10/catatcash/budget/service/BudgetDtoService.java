package com.c10.catatcash.budget.service;

import com.c10.catatcash.budget.dto.BudgetDto;
import com.c10.catatcash.budget.dto.CreateBudgetValidatorDto;

public interface BudgetDtoService {

     CreateBudgetValidatorDto isValidCreateBudget (BudgetDto budgetDto);

}
