package com.c10.catatcash.budget.service;

import com.c10.catatcash.budget.model.Budget;



public interface BudgetService {
    Budget createBudget(Budget budget);
    void deleteBudgetByID(Long id);
    Iterable <Budget> getBudgetsByUser();
    Budget getBudgetByID(Long id);
    void addSpend(Long spend, Long id);



}
