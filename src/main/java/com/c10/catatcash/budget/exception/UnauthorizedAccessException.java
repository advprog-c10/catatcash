package com.c10.catatcash.budget.exception;


public class UnauthorizedAccessException extends RuntimeException{
    public UnauthorizedAccessException(String errorMessage) {
        super(errorMessage);
    }
}