package com.c10.catatcash.wallet.service;

import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.wallet.dto.WalletDto;
import com.c10.catatcash.wallet.exception.UnauthorizedAccessException;
import com.c10.catatcash.wallet.model.Wallet;

public interface WalletUtilityService {
    User getCurrentUser();

    void validateUserOwnsWallet(Long walletId);

    void validateBalanceAmount(Wallet senderWallet, long amount);

    Wallet dtoToWallet(WalletDto walletDto);
}
