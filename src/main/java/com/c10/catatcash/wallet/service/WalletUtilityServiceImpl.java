package com.c10.catatcash.wallet.service;

import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.userauth.repository.UserRepository;
import com.c10.catatcash.wallet.dto.WalletDto;
import com.c10.catatcash.wallet.exception.NotEnoughBalanceException;
import com.c10.catatcash.wallet.exception.UnauthorizedAccessException;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class WalletUtilityServiceImpl implements WalletUtilityService{

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public User getCurrentUser() {
        String currentEmail = SecurityContextHolder.
                getContext().
                getAuthentication().
                getName();
        return userRepository.findByEmail(currentEmail);
    }

    @Override
    public void validateUserOwnsWallet(Long walletId) {
        Wallet wallet = walletRepository.findByWalletId(walletId);
        if (wallet == null || !wallet.getAppUser().getId().equals(getCurrentUser().getId()) ) {
            throw new UnauthorizedAccessException("Either this action doesn't exist or you don't have access to it");
        }
    }

    @Override
    public void validateBalanceAmount(Wallet senderWallet, long amount) {
        if (senderWallet.getBalance() - amount < 0) {
            throw new NotEnoughBalanceException("Not enough balance from " + senderWallet.getName() + " to make transfer");
        }
    }

    @Override
    public Wallet dtoToWallet(WalletDto walletDto) {
        return new Wallet(walletDto.getName(), walletDto.getBalance());
    }


}
