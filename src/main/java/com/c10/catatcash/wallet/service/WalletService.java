package com.c10.catatcash.wallet.service;

import com.c10.catatcash.wallet.exception.UnauthorizedAccessException;
import com.c10.catatcash.wallet.model.Wallet;

public interface WalletService {

    Wallet createWallet(Wallet wallet);

    Iterable<Wallet> getWallets();

    Iterable<Wallet> getWalletsExceptId(Long walletId);

    Wallet getWalletById(Long walletId);

    void deleteWalletById(Long walletId);

    void transferBalance(Wallet sender, Wallet receiver, long amount);

}
