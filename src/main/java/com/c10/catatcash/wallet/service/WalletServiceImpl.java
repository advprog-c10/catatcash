package com.c10.catatcash.wallet.service;

import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WalletServiceImpl implements WalletService {

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private WalletUtilityService walletUtilityService;


    /**
     * Creates a new wallet for the currently logged in user.
     * @return Wallet object
     */
    @Override
    public Wallet createWallet(Wallet wallet) {
        User currentUser = walletUtilityService.getCurrentUser();
        wallet.setAppUser(currentUser);
        walletRepository.save(wallet);
        return wallet;
    }

    /**
     * get all wallets from the logged in user.
     * @return list of wallets
     */
    @Override
    public Iterable<Wallet> getWallets() {
        User user = walletUtilityService.getCurrentUser();
        return walletRepository.findByAppUser(user);
    }

    public Iterable<Wallet> getWalletsExceptId(Long walletId) {
        User user = walletUtilityService.getCurrentUser();
        return walletRepository.findByAppUserAndWalletIdNot(user, walletId);
    }

    /**
     * get wallet by id.
     * @param walletId
     * @return wallet object
     */
    @Override
    public Wallet getWalletById(final Long walletId) {
        walletUtilityService.validateUserOwnsWallet(walletId);
        return walletRepository.findByWalletId(walletId);
    }

    /**
     * delete a wallet with corresponding id.
     * @param walletId
     */
    @Override
    public void deleteWalletById(final Long walletId) {
        walletUtilityService.validateUserOwnsWallet(walletId);
        walletRepository.deleteById(walletId);
    }

    /**
     * Transfer balance between two wallets
     * @param sender
     * @param receiver
     * @param amount
     */
    @Override
    public void transferBalance(Wallet sender, Wallet receiver, long amount) {
        walletUtilityService.validateUserOwnsWallet(sender.getWalletId());
        walletUtilityService.validateUserOwnsWallet(receiver.getWalletId());
        walletUtilityService.validateBalanceAmount(sender, amount);
        sender.setBalance(sender.getBalance() - amount);
        receiver.setBalance(receiver.getBalance() + amount);
        walletRepository.save(sender);
        walletRepository.save(receiver);
    }
}
