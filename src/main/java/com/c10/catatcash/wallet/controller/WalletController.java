package com.c10.catatcash.wallet.controller;

import com.c10.catatcash.wallet.dto.WalletDto;
import com.c10.catatcash.wallet.exception.UnauthorizedAccessException;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.exception.NotEnoughBalanceException;
import com.c10.catatcash.wallet.service.WalletService;
import com.c10.catatcash.wallet.service.WalletUtilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping(path = "/wallet")
public class WalletController {

    /**
     * Service.
     */
    @Autowired
    private WalletService walletService;

    @Autowired
    private WalletUtilityService walletUtilityService;

    private static final String REDIRECT_URL = "redirect:/wallet";
    private static final String ERROR_PAGE_TEMPLATE = "wallet/error_page";

    /**
     * @param model
     * @return wallet homepage
     */
    @GetMapping("")
    public String walletHome(final Model model) {
        model.addAttribute("wallets", walletService.getWallets());
        return "wallet/home";
    }

    /**
     * @param model
     * @param walletId
     * @return wallet detail view
     */
    @GetMapping(path = "/{walletId}", produces = {"application/json"})
    public String walletDetail(
            final Model model,
            final @PathVariable(value = "walletId") Long walletId) {
        try {
            model.addAttribute("wallet", walletService.getWalletById(walletId));
            return "wallet/detail";
        } catch (UnauthorizedAccessException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return  ERROR_PAGE_TEMPLATE;
        }

    }

    /**
     * @param walletId
     * @return redirect to wallet homepage
     */
    @DeleteMapping(path = "/{walletId}/delete")
    public String deleteWallet(
            final @PathVariable(value = "walletId") Long walletId,
            Model model) {
        try {
            walletService.deleteWalletById(walletId);
            return REDIRECT_URL;
        } catch (UnauthorizedAccessException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return  ERROR_PAGE_TEMPLATE;
        }
    }

    /**
     * @return form for adding new wallet
     */
    @GetMapping("/add")
    public String addWalletGet(Model model) {
        model.addAttribute("wallet",new WalletDto());
        return "wallet/wallet_form";
    }

    /**
     * @return endpoint for creating new wallet
     */
    @PostMapping("/add")
    public String addWalletPost(@ModelAttribute("wallet") WalletDto walletDto) {
        Wallet wallet = walletUtilityService.dtoToWallet(walletDto);
        walletService.createWallet(wallet);
        return REDIRECT_URL;
    }

    @GetMapping("/{walletId}/transfer")
    public String transferWalletGet(
            Model model,
            @PathVariable(value = "walletId") Long walletId) {
        try {
            model.addAttribute("wallets", walletService.getWalletsExceptId(walletId));
            model.addAttribute("walletSender", walletService.getWalletById(walletId));
            return "wallet/transfer";
        } catch (UnauthorizedAccessException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return  ERROR_PAGE_TEMPLATE;
        }
    }

    @PostMapping("/{walletId}/transfer")
    public String transferWalletPost(
            @PathVariable(value = "walletId") Long senderId,
            @RequestParam(value = "receiverId") Long receiverId,
            @RequestParam(value = "amount") long amount,
            Model model,
            RedirectAttributes redirectAttributes) {
        Wallet sender = walletService.getWalletById(senderId);
        Wallet receiver = walletService.getWalletById(receiverId);
        try {
            walletService.transferBalance(sender, receiver, amount);
            redirectAttributes.addFlashAttribute("message", "Balance transfer successful");
            return REDIRECT_URL;
        } catch (NotEnoughBalanceException e) {
            redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
            return "redirect:/wallet/"+ senderId +"/transfer";
        } catch (UnauthorizedAccessException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return  ERROR_PAGE_TEMPLATE;
        }
    }


}
