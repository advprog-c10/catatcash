package com.c10.catatcash.wallet.model;

import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.userauth.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.c10.catatcash.budget.model.Budget;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "wallet")
public class Wallet {

    /**
     * Auto generated primary key for Wallet entity.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "wallet_id", nullable = false)
    private Long walletId;

    /**
     *  wallet name.
     */
    @Column(name = "name", nullable = false)
    private String name;

    /**
     *  wallet balance.
     */
    @Column(name = "balance", nullable = false)
    private long balance;

    /**
     * relation to User entity.
     */
    @ManyToOne
    @JoinColumn(name = "app_user", nullable = false)
    private User appUser;

    @OneToMany(mappedBy = "wallet", cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Budget> budgets;


    /**
     * relation to Transaction entity.
     */
    @OneToMany(mappedBy = "wallet", cascade = CascadeType.ALL)
    private List<Transaction> transactions;

    /**
     * constructor.
     * @param name
     * @param initialBalance
     */
    public Wallet(String name, long initialBalance) {
        this.name = name;
        this.balance = initialBalance;
    }



}
