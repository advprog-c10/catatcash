package com.c10.catatcash.wallet.exception;

public class NotEnoughBalanceException extends RuntimeException{
    public NotEnoughBalanceException(String errorMessage) {
        super(errorMessage);
    }
}
