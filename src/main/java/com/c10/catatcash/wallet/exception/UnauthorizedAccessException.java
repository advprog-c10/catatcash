package com.c10.catatcash.wallet.exception;

public class UnauthorizedAccessException extends RuntimeException{
    public UnauthorizedAccessException(String errorMessage) {
        super(errorMessage);
    }
}
