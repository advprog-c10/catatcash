package com.c10.catatcash.wallet.repository;

import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.wallet.model.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {
    /**
     *
     * @param walletId
     * @return wallet object with corresponding id
     */
    Wallet findByWalletId(Long walletId);

    /**
     *
     * @param user
     * @return lists of wallets from a given user
     */
    Iterable<Wallet> findByAppUser(User user);

    /**
     *
     * @param user
     * @param walletId
     * @return list of walllets from a given user except one wallet with the given id
     */
    Iterable<Wallet> findByAppUserAndWalletIdNot(User user, Long walletId);
}
