package com.c10.catatcash.wallet.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WalletDto {
    private String name;

    private Long balance;

    public WalletDto(String name, Long balance) {
        this.name = name;
        this.balance = balance;
    }
}
