package com.c10.catatcash.userauth.service;

import com.c10.catatcash.userauth.dto.UserRegistrationDto;
import com.c10.catatcash.userauth.model.Role;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.userauth.repository.UserRepository;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private WebClient webClient;

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User save(UserRegistrationDto registrationDto) {
        var user = new User(registrationDto.getEmail(),
                passwordEncoder.encode(registrationDto.getPassword()),
                Collections.singletonList(new Role("ROLE_USER")));
        return userRepository.save(user);
    }

    @Override
    public void sendEmail(UserRegistrationDto registrationDto) {
        registrationDto.setPassword(null);
        webClient.post()
                .uri("/registerEmail")
                .body(Mono.just(registrationDto), UserRegistrationDto.class)
                .retrieve()
                .bodyToMono(UserRegistrationDto.class)
                .subscribe();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userRepository.findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(),
                user.getPassword(), mapRolesToAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }
}
