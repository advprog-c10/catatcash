package com.c10.catatcash.userauth.service;

import com.c10.catatcash.userauth.dto.UserRegistrationDto;
import com.c10.catatcash.userauth.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User save(UserRegistrationDto registrationDto);

    User findByEmail(String email);

    void sendEmail(UserRegistrationDto registrationDto);
}
