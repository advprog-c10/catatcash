package com.c10.catatcash.userauth.controller;

import com.c10.catatcash.userauth.dto.UserRegistrationDto;
import com.c10.catatcash.userauth.service.UserService;
import com.nulabinc.zxcvbn.Zxcvbn;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/register")
public class UserRegistrationController {

    private static final String REGISTRATION_TEMPLATE = "userauth/registration";

    @Autowired
    private UserService userService;

    @ModelAttribute("user")
    public UserRegistrationDto userRegistrationDto() {
        return new UserRegistrationDto();
    }

    @GetMapping
    public String register() {
        return REGISTRATION_TEMPLATE;
    }

    @PostMapping
    public String registerUserAccount(Model model, @ModelAttribute("user")
        @Valid UserRegistrationDto registrationDto) {
        var passwordCheck = new Zxcvbn();
        var existing = userService.findByEmail(registrationDto.getEmail());
        if (existing != null) {
            model.addAttribute("errorMessage",
                    "Oops!  There is already a user registered with the email provided.");
            return REGISTRATION_TEMPLATE;
        }
        var strength = passwordCheck.measure(registrationDto.getPassword());

        if (strength.getScore() < 2) {
            model.addAttribute("errorMessage", "Your password is too weak. "
                    + strength.getFeedback().getWarning());
            return REGISTRATION_TEMPLATE;
        }

        userService.save(registrationDto);
        userService.sendEmail(registrationDto);
        return "redirect:/login";
    }
}
