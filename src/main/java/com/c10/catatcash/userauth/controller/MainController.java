package com.c10.catatcash.userauth.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    private boolean checkAuthentication() {
        return !SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser");
    }

    @GetMapping("/")
    public String home() {
        if (checkAuthentication()) {
            return "redirect:/transaction";
        }
        return "userauth/home";
    }

    @GetMapping("/login")
    public String login() {
        if (checkAuthentication()) {
            return "redirect:/transaction";
        }
        return "userauth/login";
    }
}
