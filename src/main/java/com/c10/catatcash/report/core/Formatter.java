package com.c10.catatcash.report.core;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class Formatter {
    public static Timestamp[] format(String date){
        try{
            String[] dates = date.split(",");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date parsedDateBegin = dateFormat.parse(dates[0]);
            Date parsedDateEnd = dateFormat.parse(dates[1]);
            Timestamp timestampBegin = new java.sql.Timestamp(parsedDateBegin.getTime());
            Timestamp timestampEnd = new java.sql.Timestamp(parsedDateEnd.getTime());
            Timestamp[] timestamps = {timestampBegin, timestampEnd};
            return timestamps;
        }
        catch(Exception e){
            System.out.println(e);
        }
        return null;
    }

    public static LocalDateTime[] formatLocalDateTime(String date){
        Timestamp[] localdates = format(date);
        return new LocalDateTime[]{localdates[0].toLocalDateTime(), localdates[1].toLocalDateTime()};
    }
}
