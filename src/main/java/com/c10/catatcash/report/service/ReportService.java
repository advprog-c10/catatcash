package com.c10.catatcash.report.service;

import com.c10.catatcash.transaction.model.Transaction;

import java.util.List;


public interface ReportService {

    public Iterable<Transaction> makeReport(String wallet, String date);
    public String[] calculateWallet(String wallet, String date);
    public String[] calculateReport(Iterable<Transaction> transactions, String date);
    public List<List<Object>> getChartData(Iterable<Transaction> transactions, String date);
}
