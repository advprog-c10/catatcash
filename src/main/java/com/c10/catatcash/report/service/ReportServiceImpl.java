package com.c10.catatcash.report.service;

import com.c10.catatcash.report.core.Formatter;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.service.TransactionService;
import com.c10.catatcash.transaction.service.TransactionUtilityService;
import com.c10.catatcash.wallet.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.text.Format;
import java.time.LocalDateTime;
import java.util.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionUtilityService transactionUtilityService;

    @Override
    public Iterable<Transaction> makeReport(String wallet, String date) {
        Iterable<Transaction> walletReport = transactionService.getListTransactionByFilter("by-wallet", wallet
                , transactionUtilityService.getCurrentUser());
        Iterable<Transaction> dateReport = transactionService.getListTransactionByFilter("by-timestampinterval", date
                , transactionUtilityService.getCurrentUser());
        List<Transaction> reportList = new ArrayList<Transaction>();
        for(Transaction transaction : dateReport){
            for(Transaction walletTransaction : walletReport){
                if(transaction == walletTransaction) reportList.add(transaction);
            }
        }
        Iterable<Transaction> finalReport = reportList;
        return finalReport;
    }

    @Override
    public String[] calculateReport(Iterable<Transaction> transactions, String date) {
        Timestamp[] timestamps = Formatter.format(date);
        String[] report =  new String[2];
        int income = 0;
        int expense = 0;
        for(Transaction transaction : transactions){
            if(transaction.getAmount() >= 0) income += transaction.getAmount();
            else expense -= transaction.getAmount();
        }
        report[0] = "Rp " + String.format("%,d", income);
        report[1] = "Rp " + String.format("%,d", expense);
        return report;
    }

    @Override
    public String[] calculateWallet(String wallet, String date) {
        Iterable<Transaction> wallets = transactionService.getListTransactionByFilter("by-wallet", wallet
                , transactionUtilityService.getCurrentUser());
        String[] balances =  new String[2];
        long balance = 0;
        for(Transaction transaction : wallets){
            balance = transaction.getWallet().getBalance();
            break;
        }
        long balanceBegin = balance;
        long balanceEnd = balance;
            Timestamp[] timestamps = Formatter.format(date);
            for(Transaction transaction : wallets){
                if(timestamps[0].compareTo(transaction.getDate()) <= 0){
                    balanceBegin -= transaction.getAmount();}
                if(timestamps[1].compareTo(transaction.getDate()) < 0)
                    balanceEnd -= transaction.getAmount();
            }
            balances[0] = "Rp " + String.format("%,d", (int)balanceBegin);
            balances[1] = "Rp " + String.format("%,d", (int)balanceEnd);
            return balances;
    }

    @Override
    public List<List<Object>> getChartData(Iterable<Transaction> transactions, String date) {
        LocalDateTime[] dates = Formatter.formatLocalDateTime(date);
        List<List<Object>> chart;
        for(Transaction transaction : transactions){

        }
        return null;
    }
}
