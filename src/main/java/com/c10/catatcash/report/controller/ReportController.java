package com.c10.catatcash.report.controller;

import com.c10.catatcash.report.service.ReportService;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.service.TransactionService;
import com.c10.catatcash.transaction.service.TransactionUtilityService;
import com.c10.catatcash.wallet.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(path = "/report")
public class ReportController {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionUtilityService transactionUtilityService;

    @Autowired
    private WalletService walletService;

    @Autowired
    ReportService reportService;

    private static final String REDIRECT_URL = "redirect:/report";
    private static final String TRANSACTION_LIST = "transactionList";
    private static final String REPORT = "report";

    @GetMapping("")
    public String reportHome(Model model){
        if (model.getAttribute("isFiltered") == null){
            model.addAttribute("isFiltered", false);
        }
        model.addAttribute(TRANSACTION_LIST,
                transactionService.getListTransactionByFilter("by-nofilter", "", transactionUtilityService.getCurrentUser()));
        model.addAttribute("walletList", walletService.getWallets());

        return "report/home";
    }

    @GetMapping("/filter")
    public String makeReport(
            @RequestParam(value = "wallet") String wallet,
            @RequestParam(value = "date") String date,
            RedirectAttributes redirectAttributes) {
        Iterable<Transaction> reportTransaction = reportService.makeReport(wallet, date);
        redirectAttributes.addFlashAttribute(TRANSACTION_LIST,
                reportTransaction);
        redirectAttributes.addFlashAttribute(REPORT, reportService.calculateReport(reportTransaction, date));
        redirectAttributes.addFlashAttribute("balance",
                reportService.calculateWallet(wallet,date));
        redirectAttributes.addFlashAttribute("isFiltered", true);
        redirectAttributes.addFlashAttribute("date", date.split(","));
        return REDIRECT_URL;
    }

}
