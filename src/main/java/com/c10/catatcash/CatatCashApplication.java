package com.c10.catatcash;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatatCashApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatatCashApplication.class, args);
	}

}
