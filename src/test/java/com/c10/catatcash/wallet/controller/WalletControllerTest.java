package com.c10.catatcash.wallet.controller;

import com.c10.catatcash.budget.exception.UnauthorizedAccessException;
import com.c10.catatcash.userauth.service.UserService;
import com.c10.catatcash.wallet.dto.WalletDto;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.service.WalletService;
import com.c10.catatcash.wallet.service.WalletUtilityService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;


import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = WalletController.class)
@ExtendWith(MockitoExtension.class)
public class WalletControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WalletService walletService;

    @MockBean
    private WalletUtilityService walletUtilityService;

    @MockBean
    private UserService userService;

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void testGetWalletHomeAndStatusIsOk() throws Exception{
        mockMvc.perform(get("/wallet"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("walletHome"))
                .andExpect(model().attributeExists("wallets"))
                .andExpect(view().name("wallet/home"));
        verify(walletService, times(1)).getWallets();
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void testGetWalletDetailAndStatusIsOk() throws Exception {
        Wallet walletTest = mock(Wallet.class);
        walletTest.setWalletId(1L);

        when(walletService.getWalletById(walletTest.getWalletId()))
                .thenReturn(walletTest);

        mockMvc.perform(get("/wallet/" + walletTest.getWalletId()))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("walletDetail"))
                .andExpect(view().name("wallet/detail"));
    }


    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void  testWalletDeleteAndStatusIsOk() throws Exception{
        Wallet newWallet = new Wallet("wallet", 1000);
        newWallet.setWalletId(1L);

        mockMvc.perform(delete("/wallet/1/delete").with(csrf()))
                .andExpect(redirectedUrl("/wallet"))
                .andExpect(handler().methodName("deleteWallet"));
        verify(walletService,times(1)).deleteWalletById(newWallet.getWalletId());
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void testGetAddWalletAndStatusIsOk() throws Exception {
        mockMvc.perform(get("/wallet/add"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("addWalletGet"))
                .andExpect(view().name("wallet/wallet_form"));
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void testPostAddWalletAndRedirectToHome() throws Exception {
        Wallet wallet = mock(Wallet.class);
        WalletDto walletDto = new WalletDto("wallet", 100000L);

        when(walletUtilityService.dtoToWallet(walletDto))
                .thenReturn(wallet);

        when(walletService.createWallet(wallet))
                .thenReturn(wallet);

        mockMvc.perform(post("/wallet/add")
                .flashAttr("wallet", walletDto)
                .with(csrf()))
                .andExpect(redirectedUrl("/wallet"))
                .andExpect(handler().methodName("addWalletPost"));

        verify(walletService, times(1)).createWallet(wallet);
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void testGetRequestCreateWalletThereExistAttributeWallet() throws Exception {
        mockMvc.perform(get("/wallet/add"))
                .andExpect(handler().methodName("addWalletGet"))
                .andExpect(model().attributeExists("wallet"));
    }



    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void testGetWalletTransferAndStatusIsOk() throws Exception {
        Wallet walletTest = mock(Wallet.class);
        walletTest.setWalletId(1L);

        when(walletService.getWalletById(walletTest.getWalletId()))
                .thenReturn(walletTest);
        when(walletService.getWalletsExceptId(walletTest.getWalletId()))
                .thenReturn(new ArrayList<>());


        mockMvc.perform(get("/wallet/" + walletTest.getWalletId() + "/transfer"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("transferWalletGet"))
                .andExpect(view().name("wallet/transfer"));
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void testPostWalletTransferAndRedirectToHomePage() throws Exception {
        Wallet walletSender = mock(Wallet.class);
        walletSender.setWalletId(1L);
        walletSender.setBalance(10000);
        Wallet walletReceiver = mock(Wallet.class);
        walletReceiver.setWalletId(2L);

        when(walletService.getWalletById(walletSender.getWalletId()))
                .thenReturn(walletSender);
        when(walletService.getWalletById(walletReceiver.getWalletId()))
                .thenReturn(walletReceiver);

        mockMvc.perform(post("/wallet/1/transfer")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"receiverId\": 2 , \"amount\": 10000 }")
                .param("receiverId", "2")
                .param("amount", "10000")
                .with(csrf()))
                .andExpect(redirectedUrl("/wallet"))
                .andExpect(handler().methodName("transferWalletPost"));
    }
}
