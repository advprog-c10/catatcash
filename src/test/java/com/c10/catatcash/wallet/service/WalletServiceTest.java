package com.c10.catatcash.wallet.service;

import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.repository.WalletRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class WalletServiceTest {

    @Mock
    private WalletRepository walletRepository;

    @Mock
    private WalletUtilityService walletUtilityService;

    @InjectMocks
    private WalletServiceImpl walletService;

    private Wallet wallet;
    private User user;

    @BeforeEach
    public void setUp() {
        wallet = mock(Wallet.class);
        user = mock(User.class);
    }

    @Test
    void createWalletTest() {
        when(walletUtilityService.getCurrentUser())
                .thenReturn(user);

        walletService.createWallet(wallet);

        verify(walletRepository, times(1)).save(wallet);
    }

    @Test
    void testGetWallets() {
        when(walletUtilityService.getCurrentUser())
                .thenReturn(user);

        walletService.getWallets();

        verify(walletRepository, times(1)).findByAppUser(user);
    }

    @Test
    void testGetWalletsExceptId() {
        when(walletUtilityService.getCurrentUser())
                .thenReturn(user);

        walletService.getWalletsExceptId(wallet.getWalletId());

        verify(walletRepository, times(1)).findByAppUserAndWalletIdNot(user, wallet.getWalletId());
    }

    @Test
    void testGetWalletById() {
        Long walletId = wallet.getWalletId();
        doNothing().when(walletUtilityService).validateUserOwnsWallet(walletId);

        walletService.getWalletById(walletId);

        verify(walletRepository, times(1)).findByWalletId(walletId);
    }

    @Test
    void testDeleteWalletById() {
        Long walletId = wallet.getWalletId();
        doNothing().when(walletUtilityService).validateUserOwnsWallet(walletId);

        walletService.deleteWalletById(walletId);

        verify(walletRepository, times(1)).deleteById(walletId);
    }

    @Test
    void testTransferWallet() throws Exception {
        Wallet walletSender = mock(Wallet.class);
        Wallet walletReceiver = mock(Wallet.class);
        long amount = 100000L;

        walletService.transferBalance(walletSender, walletReceiver, amount);

        verify(walletRepository, times(1)).save(walletSender);
        verify(walletRepository, times(1)).save(walletReceiver);
    }
}
