package com.c10.catatcash.wallet.service;

import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.userauth.repository.UserRepository;
import com.c10.catatcash.wallet.dto.WalletDto;
import com.c10.catatcash.wallet.exception.NotEnoughBalanceException;
import com.c10.catatcash.wallet.exception.UnauthorizedAccessException;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.repository.WalletRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class WalletUtilityServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private WalletRepository walletRepository;

    @InjectMocks
    private WalletUtilityServiceImpl walletUtilityService;


    @Test
    void testGetCurrentUser() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(authentication.getName()).thenReturn("test@mail.com");

        walletUtilityService.getCurrentUser();

        verify(userRepository, times(1)).findByEmail("test@mail.com");
    }

    @Test
    void testInvalidBalanceAmountThrowsException() {
        Wallet senderWallet = new Wallet("sender", 10000L);
        long amount = 20000L;
        String walletName = senderWallet.getName();

        try {
            walletUtilityService.validateBalanceAmount(senderWallet,amount);
            fail("Expected a NotEnoughBalanceException to be thrown");
        } catch (NotEnoughBalanceException e) {
            assertEquals("Not enough balance from " + walletName + " to make transfer", e.getMessage());
        }
    }

    @Test
    void testValidateUserOwnsWalletThrowsException() {
        // wallet without user
        User user = mock(User.class);
        Wallet usersWallet = new Wallet();
        usersWallet.setName("usersWallet");
        usersWallet.setBalance(10000L);
        usersWallet.setWalletId(1L);
        usersWallet.setAppUser(user);
        Long walletId = usersWallet.getWalletId();

        when(walletRepository.findByWalletId(usersWallet.getWalletId())).thenReturn(null);

        try {
            walletUtilityService.validateUserOwnsWallet(walletId);
            fail("Expected an UnauthorizedAccessException to be thrown");
        } catch (UnauthorizedAccessException e) {
            assertEquals( "Either this action doesn't exist or you don't have access to it", e.getMessage());
        }

    }

    @Test
    void testDtoToWallet() {
        WalletDto walletDto = new WalletDto("wallet", 10000L);
        Wallet wallet =  walletUtilityService.dtoToWallet(walletDto);
        assertEquals(wallet.getName(), walletDto.getName());
        assertEquals(wallet.getBalance(), walletDto.getBalance());
    }

}
