package com.c10.catatcash.budget.controller;


import com.c10.catatcash.budget.dto.BudgetDto;
import com.c10.catatcash.budget.dto.CreateBudgetValidatorDto;
import com.c10.catatcash.budget.exception.UnauthorizedAccessException;
import com.c10.catatcash.budget.model.Budget;
import com.c10.catatcash.budget.repository.BudgetRepository;
import com.c10.catatcash.budget.service.BudgetDtoService;
import com.c10.catatcash.budget.service.BudgetService;

import com.c10.catatcash.budget.service.BudgetUtilityService;
import com.c10.catatcash.userauth.model.Role;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.userauth.service.UserService;

import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.service.WalletService;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Date;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.mockito.Mockito.*;


@WebMvcTest(controllers = BudgetController.class)
@ExtendWith(MockitoExtension.class)
 class BudgetControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BudgetService budgetService;

    @MockBean
    private BudgetDtoService budgetDtoService;

    @MockBean
    private WalletService walletService;

    @MockBean
    private UserService userService;

    @MockBean
    private BudgetUtilityService budgetUtilityService;

    @MockBean
    private BudgetRepository budgetRepository;




    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
     void  testBudgetHomeOk() throws Exception{
        Wallet newWallet = mock(Wallet.class);
        when(walletService.getWallets()).thenReturn(Collections.singletonList(newWallet));
        this.mvc.perform(get("/budget"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("budgetHome"))
                .andExpect(model().attributeExists("budgets"))
                .andExpect(view().name("budget/homeBudget"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/budget", "/budget/2", "/budget/add"})
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void  testBudgetControllerRedirectToWallet(String url) throws Exception{
        when(walletService.getWallets()).thenReturn(Collections.emptyList());
        this.mvc.perform(get(url))
                .andExpect(status().isFound());
    }




    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void  testBudgeDetailOk() throws Exception{
        Wallet newWallet = mock(Wallet.class);
        Date startDate = mock(Date.class);
        Date endDate = mock(Date.class);
        Budget newBudget = new Budget(newWallet, "testgg", 1L , startDate, endDate);
        newBudget.setId(2L);
        newBudget.setSpend(0L);
        when(walletService.getWallets()).thenReturn(Collections.singletonList(newWallet));
        when(budgetService.getBudgetByID(2L)).thenReturn(newBudget);
        this.mvc.perform(get("/budget/2"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("budgetDetail"))
                .andExpect(model().attributeExists("budget"))
                .andExpect(view().name("budget/detail"));
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void  testBudgeDetailBudgetNotFound() throws Exception{
        Wallet newWallet = mock(Wallet.class);
        Date startDate = mock(Date.class);
        Date endDate = mock(Date.class);
        Budget newBudget = new Budget(newWallet, "testgg", 1L , startDate, endDate);
        newBudget.setId(2L);
        newBudget.setSpend(0L);
        when(walletService.getWallets()).thenReturn(Collections.singletonList(newWallet));
        when(budgetService.getBudgetByID(2L)).thenReturn(newBudget);
        this.mvc.perform(get("/budget/3"))
                .andExpect(status().isFound());
    }

//    @Test
//    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
//    void  testBudgeDetailWhenUserisDoesntAccess() throws Exception{
//        Wallet newWallet = new Wallet("usersWallet", 10000L);
//        Date startDate = mock(Date.class);
//        Date endDate = mock(Date.class);
//        User user = new User(
//                "advprogc10@gmail.com",
//                "4ea1be751a72",
//                Collections.singletonList(new Role("ROLE_USER"))
//        );
//        user.setId(2L);
//        Budget newBudget = new Budget(newWallet, "testgg", 1l , startDate, endDate);
//        newBudget.setId(2l);
//        newBudget.setSpend(0l);
//        newWallet.setAppUser(user);
//        when(budgetRepository.findByid(anyLong())).thenReturn(newBudget);
//        when(walletService.getWallets()).thenReturn(Collections.singletonList(newWallet));
//        doThrow(new UnauthorizedAccessException("you don't have permission to access this budget")).
//                when(budgetService).getBudgetByID(anyLong());
//        this.mvc.perform(get("/budget/2"))
//                .andExpect(status().isFound());
//    }







    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void  testGetRequestCreateBudgetReturnBudgetDto() throws Exception{
        Wallet newWallet = mock(Wallet.class);
        when(walletService.getWallets()).thenReturn(Collections.singletonList(newWallet));

        this.mvc.perform(get("/budget/add"))
                .andExpect(handler().methodName("addBudgetGet"))
                .andExpect(model().attributeExists("newBudget"))
                .andExpect(model().attributeExists("wallets"));
    }


    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void  testBudgeDeleteOk() throws Exception{
        Wallet newWallet = mock(Wallet.class);
        Date startDate = mock(Date.class);
        Date endDate = mock(Date.class);
        Budget newBudget = new Budget(newWallet, "testgg", 1L , startDate, endDate);
        newBudget.setId(2L);
        this.mvc.perform(delete("/budget/2/delete").with(csrf()));
        verify(budgetService,times(1)).deleteBudgetByID(newBudget.getId());
    }

//    @Test
//    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
//    void  testBudgeDeleteNotOk() throws Exception{
//        Wallet newWallet = mock(Wallet.class);
//        Date startDate = mock(Date.class);
//        Date endDate = mock(Date.class);
//        Budget newBudget = new Budget(newWallet, "testgg", 1l , startDate, endDate);
//        newBudget.setId(2l);
//        budgetService.deleteBudgetByID(newBudget.getId());
//        this.mvc.perform(delete("/budget/2/delete").with(csrf()))
//                .andExpect(status().isFound());
//
//    }

   @Test
   @WithMockUser(username = "user1", password = "pwd", roles = "USER")
   void  testPostRequestCreateBudgetAndRedirectToHome() throws Exception{
      BudgetDto newBudgetDto = mock(BudgetDto.class);
      Budget newBudget = mock(Budget.class);
      CreateBudgetValidatorDto valid = mock(CreateBudgetValidatorDto.class);

      when(budgetDtoService.isValidCreateBudget(any()))
              .thenReturn(valid);
      when(valid.getBudget())
              .thenReturn(newBudget);

      this.mvc.perform(post("/budget/add")
              .flashAttr("newBudgetDto",newBudgetDto)
              .with((csrf())))
              .andExpect(redirectedUrl("/budget"))
              .andExpect(handler().methodName("budgetSumbit"));

      verify(budgetService,times(1)).createBudget(newBudget);
   }



}
