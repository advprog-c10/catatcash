package com.c10.catatcash.budget.service;


import com.c10.catatcash.budget.dto.BudgetDto;
import com.c10.catatcash.budget.model.Budget;
import com.c10.catatcash.budget.repository.BudgetRepository;
import com.c10.catatcash.userauth.model.Role;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.userauth.repository.UserRepository;
import com.c10.catatcash.budget.exception.UnauthorizedAccessException;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.service.WalletService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
 class BudgetUtiltyServiceImplTest {

    @Mock
    private UserRepository userRepository;



    @Mock
    private WalletService walletService;

    @Mock
    private BudgetRepository budgetRepository;

    @InjectMocks
    private BudgetUtilityServiceImpl budgetUtilityService;

    @Test
    void dtoToBudgetReturnBudget() throws  Exception{
        Wallet wallet = new Wallet();
        String name = "mantap";
        Long amount = 10000L;
        Date start = new SimpleDateFormat("yyyy-MM-dd").parse("2021-01-01");
        Date end = new SimpleDateFormat("yyyy-MM-dd").parse("2021-01-05");

        BudgetDto budgetDto = mock(BudgetDto.class);
        when(budgetDto.getWallet()).thenReturn(wallet);
        when(budgetDto.getName()).thenReturn(name);
        when(budgetDto.getAmount()).thenReturn(amount);
        when(budgetDto.getStart()).thenReturn(start);
        when(budgetDto.getEnd()).thenReturn(end);

        assertTrue(budgetUtilityService.dtoToBudget(budgetDto) instanceof Budget);
    }
    @Test
     void methodIsRequestMadeByWalletOwnerReturnBudget() {
        Budget budget = mock(Budget.class);
        Wallet wallet = mock(Wallet.class);
        List<Wallet> listWallet = new ArrayList<Wallet>();
        listWallet.add(wallet);
        when(walletService.getWallets())
                .thenReturn(listWallet);
        when(budget.getWallet())
                .thenReturn(wallet);
        when(wallet.getWalletId())
                .thenReturn(1L);
        Budget returned = budgetUtilityService.isRequestMadeByWalletOwner(budget);
        assertEquals(budget, returned);
    }

    @Test
     void methodIsRequestMadeByWalletOwnerReturnNullBecauseBudgetNotMadeByUser() {
        Budget budget = mock(Budget.class);
        Wallet wallet = mock(Wallet.class);
        Wallet anotherWallet = mock(Wallet.class);
        List<Wallet> listWallet = new ArrayList<Wallet>();
        listWallet.add(wallet);
        when(walletService.getWallets())
                .thenReturn(listWallet);
        when(budget.getWallet())
                .thenReturn(anotherWallet);
        when(anotherWallet.getWalletId())
                .thenReturn(2L);
        when(wallet.getWalletId())
                .thenReturn(1L);
        Budget returned = budgetUtilityService.isRequestMadeByWalletOwner(budget);
        assertEquals(null, returned);
    }
    @Test
    void  testGetCurrentUser(){
       User user = mock(User.class);
       Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        when(authentication.getName())
                .thenReturn("dummy");
        when(userRepository.findByEmail(any(String.class)))
                .thenReturn(user);
        User returned = budgetUtilityService.getCurrentUser();
        assertEquals(user, returned);
    }

    @Test
    void testValidateUserOwnsBudgetThrowsException(){
        Budget budget = mock(Budget.class);
        Long id = budget.getId();
        when(budgetRepository.findByid(anyLong())).thenReturn(budget);
        assertThrows(UnauthorizedAccessException.class, () -> {
            budgetUtilityService.validateUserOwnsBudget(id);
        });

    }

    @Test
    void testValidateUserOwnsBudgetNull(){
        Budget budget = mock(Budget.class);
        Long id = budget.getId();
        when(budgetRepository.findByid(anyLong())).thenReturn(null);
        budgetUtilityService.validateUserOwnsBudget(id);
    }

    @Test
    void testValidateUserOwnsBudgetDontThrowsException(){
       User user = new User(
                "advprogc10@gmail.com",
                "4ea1be751a72",
                Collections.singletonList(new Role("ROLE_USER"))
        );
       user.setId(2L);
        Wallet usersWallet = new Wallet("usersWallet", 10000L);
        usersWallet.setAppUser(user);
        Budget budget = mock(Budget.class);
        budget.setWallet(usersWallet);
        when(budgetRepository.findByid(anyLong())).thenReturn(budget);
        when(budgetUtilityService.getCurrentUser()).thenReturn(user);
        when(budget.getWallet()).thenReturn(usersWallet);
        budgetUtilityService.validateUserOwnsBudget(budget.getId());
        verify(budgetRepository,times(1)).findByid(budget.getId());
    }
}
