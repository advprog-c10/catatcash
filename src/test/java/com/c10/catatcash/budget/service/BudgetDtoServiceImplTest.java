package com.c10.catatcash.budget.service;


import com.c10.catatcash.budget.dto.BudgetDto;
import com.c10.catatcash.budget.dto.CreateBudgetValidatorDto;
import com.c10.catatcash.budget.model.Budget;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
 class BudgetDtoServiceImplTest {



    @Mock
    private BudgetUtilityService budgetUtilityService;

    @InjectMocks
    private BudgetDtoServiceImpl budgetDtoService;

    private BudgetDto budgetDto;
    private Budget budget;

    @BeforeEach
    public  void  setUp(){
        budgetDto = mock(BudgetDto.class);
        budget = mock(Budget.class);
    }

    @Test
    void createBudgetValidatorDtoTest(){
        when(budgetUtilityService.dtoToBudget(budgetDto)).thenReturn(budget);
        when(budgetUtilityService.isRequestMadeByWalletOwner(budget)).thenReturn(budget);

        CreateBudgetValidatorDto returned =budgetDtoService.isValidCreateBudget(budgetDto);
        assertEquals(budget, returned.getBudget());
    }

}
