package com.c10.catatcash.budget.service;


import com.c10.catatcash.budget.model.Budget;
import com.c10.catatcash.budget.repository.BudgetRepository;
import com.c10.catatcash.transaction.service.TransactionUtilityService;
import com.c10.catatcash.userauth.model.User;

import com.c10.catatcash.wallet.model.Wallet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
 class BudgetServiceImplTest {

    @Mock
    private BudgetRepository budgetRepository;


    @Mock
    private TransactionUtilityService transactionUtilityService;

    @Mock
    private BudgetUtilityService budgetUtilityService;

    @InjectMocks
    private BudgetServiceImpl budgetService;

    private Budget budget;
    private Wallet wallet;
    private Long walletId;
    private User user;


@BeforeEach
    void setUP(){
    budget = mock(Budget.class);
    wallet = mock(Wallet.class);
    user = mock(User.class);
    walletId = 1L;

}

@Test
    void createBudgetTest(){

    budgetService.createBudget(budget);

    verify(budgetRepository, times(1)).save(budget);
}

@Test
    void deleteBudgetTest(){
    budgetService.deleteBudgetByID(budget.getId());

    verify(budgetRepository,times(1)).deleteById(budget.getId());
}

@Test
    void getBudgetsTest(){
    when(budgetUtilityService.getCurrentUser()).thenReturn(user);
    budgetService.getBudgetsByUser();

    verify(budgetRepository,times(1)).findByWalletAppUser(user);
}

@Test
    void getBudgetById(){
    Long id = budget.getId();
    budgetUtilityService.validateUserOwnsBudget(id);
     budgetService.getBudgetByID(id);

     verify(budgetRepository,times(1)).findByid(id);
}

@Test
    void addSpendTest(){
    Date startDate = mock(Date.class);
    Date endDate = mock(Date.class);
    Budget newBudget = new Budget(wallet, "testgg", 1L , startDate, endDate);
    newBudget.setId(2L);
    newBudget.setSpend(0L);
    budgetUtilityService.validateUserOwnsBudget(2L);
    when(budgetService.getBudgetByID(2L)).thenReturn(newBudget);
    budgetService.addSpend(2L,newBudget.getId());
    assertEquals(2L, newBudget.getSpend());
}
}
