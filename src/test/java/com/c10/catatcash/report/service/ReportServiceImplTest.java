//package com.c10.catatcash.report.service;
//
//import com.c10.catatcash.report.service.ReportService;
//import com.c10.catatcash.transaction.model.Transaction;
//import com.c10.catatcash.transaction.repository.FilterStrategyRepository;
//import com.c10.catatcash.transaction.repository.TransactionRepository;
//import com.c10.catatcash.transaction.service.TransactionServiceImpl;
//import com.c10.catatcash.transaction.service.TransactionUtilityService;
//import com.c10.catatcash.wallet.model.Wallet;
//import com.c10.catatcash.userauth.model.User;
//import com.c10.catatcash.wallet.service.WalletService;
//
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import static org.mockito.Mockito.*;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@ExtendWith(MockitoExtension.class)
//public class ReportServiceImplTest {
//
//    @Mock
//    private FilterStrategyRepository filterStrategyRepository;
//
//    @Mock
//    private TransactionRepository transactionRepository;
//
//    @Mock
//    private WalletService walletService;
//
//    @Mock
//    private TransactionUtilityService transactionUtilityService;
//
//    @InjectMocks
//    private TransactionServiceImpl transactionService;
//    private ReportService reportService;
//
//    private Transaction transaction;
//    private Timestamp[] timestamps;
//    private Iterable<Transaction> transactions;
//    private Wallet wallet;
//    private Long walletId;
//    private User user;
//    private String filterType;
//    private String filterParam;
//    private String date;
//    private String walletName;
//
//    @BeforeEach
//    public void setUp() {
//        transaction = mock(Transaction.class);
//        wallet = mock(Wallet.class);
//        user = mock(User.class);
//        walletId = 1L;
//        date = "dummy";
//        transactions = mock();
//
//        walletName = "dummy";
//        filterType = "dummy";
//        filterParam = "dummy";
//    }
//
//    @Test
//    public void makeReportTest() {
//        reportService.makeReport(walletName, date);
//
//        verify(transactionService, times(2)).getListTransactionByFilter(filterType,filterParam);
//    }
//
//    public static <T extends Iterable<I>, I> T mock(Class<T> classToMock, I first, I... others) {
//
//        Boolean[] hasNextReturn = new Boolean[others.length + 1];
//
//        for (int i = 0; i < others.length; i++)
//            hasNextReturn[i] = Boolean.TRUE;
//
//        hasNextReturn[others.length] = Boolean.FALSE;
//
//        T iterable = Mockito.mock(classToMock);
//        Iterator<I> iterator = Mockito.mock(Iterator.class);
//
//        Mockito.when(iterator.hasNext()).thenReturn(Boolean.TRUE, hasNextReturn);
//        Mockito.when(iterator.next()).thenReturn(first, others);
//
//        Mockito.when(iterable.iterator()).thenReturn(iterator);
//
//        return iterable;
//    }
//}
