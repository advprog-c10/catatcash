package com.c10.catatcash.report.controller;

import com.c10.catatcash.report.service.ReportService;
import com.c10.catatcash.transaction.dto.CreateTransactionValidatorDto;
import com.c10.catatcash.transaction.dto.DeleteTransactionValidatorDto;
import com.c10.catatcash.transaction.dto.TransactionDto;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.service.TransactionService;
import com.c10.catatcash.transaction.service.TransactionUtilityService;
import com.c10.catatcash.transaction.service.TransactionValidatorService;
import com.c10.catatcash.userauth.service.UserService;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.service.WalletService;

import java.util.List;
import java.util.ArrayList;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Test;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.mockito.Mockito.*;

@WebMvcTest(controllers = ReportController.class)
@ExtendWith(MockitoExtension.class)
public class ReportControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ReportService reportService;

    @MockBean
    private TransactionUtilityService transactionUtilityService;

    @MockBean
    private TransactionService transactionService;

    @MockBean
    private WalletService walletService;

    @MockBean
    private UserService userService;

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    public void testReportHomeBeOk() throws Exception {
        this.mvc.perform(get("/report"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("reportHome"))
                .andExpect(model().attributeExists("transactionList"))
                .andExpect(model().attributeExists("walletList"))
                .andExpect(view().name("report/home"));
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    public void testReportHomeBeOkAndMakeReportNotNull() throws Exception {
        Wallet wallet = mock(Wallet.class);
        List<Transaction> transactionList = new ArrayList<>();

        this.mvc.perform(get("/report")
                .flashAttr("isFiltered", false)
                .flashAttr("transactionList", transactionList))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("reportHome"))
                .andExpect(model().attributeExists("transactionList"))
                .andExpect(model().attributeExists("walletList"))
                .andExpect(view().name("report/home"));
        verify(reportService, times(0)).makeReport("dummy", "dummy");
    }
}
