package com.c10.catatcash.transaction.model;

import com.c10.catatcash.budget.model.Budget;
import com.c10.catatcash.wallet.model.Wallet;
import java.util.Date;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransactionTest {
  @Test
  public void testTransactionAmountNegativeWhenTypeIsExpense() {
    Wallet wallet = mock(Wallet.class);
    Budget budget = mock(Budget.class);
    String description = "";
    Long amount = 1L;
    Date date = mock(Date.class);
    Transaction transaction = new Transaction(wallet, budget,description, amount, date, TransactionType.EXPENSE);
    assertEquals(-1L * amount, transaction.getAmount());
  }

  public void testTransactionAmountPositiveWhenTypeIsIncome() {
    Wallet wallet = mock(Wallet.class);
    Budget budget = mock(Budget.class);
    String description = "";
    Long amount = 1L;
    Date date = mock(Date.class);
    Transaction transaction = new Transaction(wallet, budget, description, amount, date, TransactionType.INCOME);
    assertEquals(amount, transaction.getAmount());
  }
}