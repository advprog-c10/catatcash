package com.c10.catatcash.transaction.service;

import com.c10.catatcash.budget.model.Budget;
import com.c10.catatcash.budget.service.BudgetService;
import com.c10.catatcash.transaction.dto.TransactionDto;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.model.TransactionType;
import com.c10.catatcash.transaction.repository.TransactionRepository;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.userauth.repository.UserRepository;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.repository.WalletRepository;
import com.c10.catatcash.wallet.service.WalletService;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class TransactionUtilityServiceImplTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private WalletRepository walletRepository;

  @Mock
  private WalletService walletService;

  @Mock
  private BudgetService budgetService;

  @Mock
  private TransactionRepository transactionRepository;

  @InjectMocks
  private TransactionUtilityServiceImpl transactionUtilityService;

  @Test
  public void dtoToTransactionReturnTransaction() throws Exception {
    Wallet wallet = new Wallet();
    Budget budget = mock(Budget.class);
    String description = "description";
    Long amount = 10000L;
    Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2021-01-01");
    TransactionType type = TransactionType.INCOME;
    
    TransactionDto transactionDto = mock(TransactionDto.class);

    Transaction transaction = new Transaction(wallet, budget, description, amount, date, type);
    when(transactionDto.getWallet()).thenReturn(wallet);
    when(transactionDto.getDescription()).thenReturn(description);
    when(transactionDto.getAmount()).thenReturn(amount);
    when(transactionDto.getDate()).thenReturn(date);
    when(transactionDto.getType()).thenReturn(type);

    assertTrue(transactionUtilityService.dtoToTransaction(transactionDto) instanceof Transaction);
  }

  @Test
  public void methodIsEveryWalletBalanceGreaterOrEqualZeroReturnTrue() {
    List<Long> walletBalance = new ArrayList<>();
    walletBalance.add(10000L);
    assertTrue(transactionUtilityService.isEveryWalletBalanceGreaterOrEqualZero(walletBalance));
  }

  @Test
  public void methodIsEveryWalletBalanceGreaterOrEqualZeroReturnFalse() {
    List<Long> walletBalance = new ArrayList<>();
    walletBalance.add(-10000L);
    assertTrue(!transactionUtilityService.isEveryWalletBalanceGreaterOrEqualZero(walletBalance));
  }

  @Test
  public void methodIsRequestMadeByWalletOwnerReturnTransaction() {
    Transaction transaction = mock(Transaction.class);
    Wallet wallet = mock(Wallet.class);
    List<Wallet> listWallet = new ArrayList<Wallet>();
    listWallet.add(wallet);
    when(walletService.getWallets())
        .thenReturn(listWallet);
    when(transaction.getWallet())
        .thenReturn(wallet);
    when(wallet.getWalletId())
        .thenReturn(1L);
    Transaction returned = transactionUtilityService.isRequestMadeByWalletOwner(transaction);
    assertEquals(transaction, returned);
  }

  @Test
  public void methodIsRequestMadeByWalletOwnerReturnNullBecauseTransactionNotMadeByUser() {
    Transaction transaction = mock(Transaction.class);
    Wallet wallet = mock(Wallet.class);
    Wallet anotherWallet = mock(Wallet.class);
    List<Wallet> listWallet = new ArrayList<Wallet>();
    listWallet.add(wallet);
    when(walletService.getWallets())
        .thenReturn(listWallet);
    when(transaction.getWallet())
        .thenReturn(anotherWallet);
    when(anotherWallet.getWalletId())
        .thenReturn(2L);
    when(wallet.getWalletId())
        .thenReturn(1L);
    Transaction returned = transactionUtilityService.isRequestMadeByWalletOwner(transaction);
    assertEquals(null, returned);
  }
  @Test
  public void methodIsRequestMadeByWalletOwnerReturnNullBecauseNoWalletFromUser() {
    Transaction transaction = mock(Transaction.class);
    when(walletService.getWallets())
        .thenReturn(new ArrayList<Wallet>());
    Transaction returned = transactionUtilityService.isRequestMadeByWalletOwner(transaction);
    assertEquals(null, returned);
  }

  @Test
  public void methodGetCurrentUserReturnUser() {
    User user = mock(User.class);
    Authentication authentication = mock(Authentication.class);
    SecurityContext securityContext = mock(SecurityContext.class);
    when(securityContext.getAuthentication()).thenReturn(authentication);
    SecurityContextHolder.setContext(securityContext);

    when(authentication.getName())
        .thenReturn("dummy");
    when(userRepository.findByEmail(any(String.class)))
        .thenReturn(user);
    User returned = transactionUtilityService.getCurrentUser();
    assertEquals(user, returned);
  }

  @Test
  public void methodSetBalanceUpdateWalletBalanceWhenItIsCreateTransaction() {
    Wallet wallet = mock(Wallet.class);
    Transaction transaction = mock(Transaction.class);
    Budget budget = mock(Budget.class);
    when(transaction.getAmount())
        .thenReturn(1L);
    when(transaction.getBudget())
        .thenReturn(budget);
    when(budget.getId())
        .thenReturn(1L);
    when(wallet.getBalance())
        .thenReturn(0L);

    transactionUtilityService.setBalance(wallet, transaction, true);

    verify(wallet, times(1)).setBalance(any(Long.class));
    verify(budgetService, times(1)).addSpend(any(Long.class), any(Long.class));
    verify(walletRepository, times(1)).save(wallet);
  }

  @Test
  public void methodSetBalanceUpdateWalletBalanceWhenItIsDeleteTransaction() {
    Wallet wallet = mock(Wallet.class);
    Transaction transaction = mock(Transaction.class);
    when(transaction.getAmount())
        .thenReturn(1L);
    when(wallet.getBalance())
        .thenReturn(0L);

    transactionUtilityService.setBalance(wallet, transaction, false);

    verify(wallet, times(1)).setBalance(any(Long.class));
    verify(walletRepository, times(1)).save(wallet);
  }

  @Test
  public void getTransactionListByIdInReturnTransactionList() {
    List<Long> transactionIdList = new ArrayList<>();
    Transaction transaction = mock(Transaction.class);
    List<Transaction> transactionIterable = new ArrayList<>();
    transactionIterable.add(transaction);
    transactionIdList.add(1L);
    when(transactionRepository.findByIdIn(transactionIdList))
        .thenReturn(transactionIterable);

    List<Transaction> returned = transactionUtilityService.getTransactionListByIdIn(transactionIdList);
    assertEquals(transaction, returned.get(0));
  }

  @Test
  public void getBalanceMapReturnMap() {
    List<Wallet> walletIterable = new ArrayList<>();
    Wallet wallet = mock(Wallet.class);
    Long walletId = 1L;
    Long walletBalance = 10L;
    walletIterable.add(wallet);
    when(walletService.getWallets())
        .thenReturn(walletIterable);
    when(wallet.getWalletId())
        .thenReturn(walletId);
    when(wallet.getBalance())
        .thenReturn(walletBalance);
    Map<Long, Long> returned = transactionUtilityService.getBalanceMap();
    assertEquals(walletBalance, returned.get(walletId));
  }

  @Test
  public void getTicketForTransactionListReturnDifferentTicket() {
    String ticket1 = transactionUtilityService.getTicketForTransactionList();
    String ticket2 = transactionUtilityService.getTicketForTransactionList();
    assertNotEquals(ticket1, ticket2);
  }

  @Test
  public void deleteBudgetFromTransactionByBudgetIdRemoveBudgetFromTransaction() throws Exception {
    User user = mock(User.class);
    Long id = 1L;
    Transaction transaction = mock(Transaction.class);
    List<Transaction> transactionIterable = new ArrayList<>();
    transactionIterable.add(transaction);

    when(transactionUtilityService.getCurrentUser())
        .thenReturn(user);
    when(transactionRepository.findByBudgetIdAndWalletAppUser(id, user))
        .thenReturn(transactionIterable);

    transactionUtilityService.deleteBudgetFromTransactionByBudgetId(id);

    verify(transaction, times(1)).setBudget(null);
    verify(transactionRepository).save(transaction);
  }
}