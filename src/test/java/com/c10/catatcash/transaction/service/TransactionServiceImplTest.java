package com.c10.catatcash.transaction.service;

import com.c10.catatcash.transaction.dto.TransactionListDto;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.model.TransactionType;
import com.c10.catatcash.transaction.repository.FilterStrategyRepository;
import com.c10.catatcash.transaction.repository.TransactionRepository;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.wallet.service.WalletService;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceImplTest {

  @Mock
  private FilterStrategyRepository filterStrategyRepository;

  @Mock
  private TransactionRepository transactionRepository;

  @Mock
  private WalletService walletService;

  @Mock
  private TransactionUtilityService transactionUtilityService;

  @InjectMocks
  private TransactionServiceImpl transactionService;

  private Transaction transaction;
  private Wallet wallet;
  private Long walletId;
  private User user;
  private String filterType;
  private String filterParam;

  @BeforeEach
  public void setUp() {
    transaction = mock(Transaction.class);
    wallet = mock(Wallet.class);
    user = mock(User.class);
    walletId = 1L;
    filterType = "dummy";
    filterParam = "dummy";
  }

  @Test
  public void createTransactionTest() {
    when(transaction.getWallet())
        .thenReturn(wallet);
    when(wallet.getWalletId())
        .thenReturn(walletId);
    when(walletService.getWalletById(walletId))
        .thenReturn(wallet);

    transactionService.createTransaction(transaction);
    
    verify(transactionRepository, times(1)).save(transaction);
    verify(transactionUtilityService, times(1)).setBalance(wallet, transaction, true);
  }

  @Test
  public void deleteTransactionTest() {
    when(transaction.getWallet())
        .thenReturn(wallet);

    List<Transaction> transactionList = new ArrayList<>();
    transactionList.add(transaction);

    transactionService.deleteTransactions(transactionList);
    
    verify(transactionRepository, times(1)).delete(transaction);
    verify(transactionUtilityService, times(1)).setBalance(wallet, transaction, false);
  }

  @Test
  public void getListTransactionByFilterTest() {
    List<Transaction> transactionList = new ArrayList<>();
    transactionList.add(transaction);
    when(filterStrategyRepository.getFilteredTransaction(filterType, filterParam, user))
        .thenReturn(transactionList);

    transactionService.getListTransactionByFilter(filterType, filterParam, user);

    verify(filterStrategyRepository, times(1)).getFilteredTransaction(filterType, filterParam, user);
  }

  @Test
  public void getTicketForGetListTransactionByFilterReturnTicket() {
    String ticket = "AAAA";
    when(transactionUtilityService.getTicketForTransactionList())
        .thenReturn(ticket);
    when(transactionUtilityService.getCurrentUser())
        .thenReturn(user);

    assertEquals(ticket, transactionService.getTicketForGetListTransactionByFilter("by-nofilter", ""));

    verify(transactionUtilityService, times(1)).getTicketForTransactionList();
  }

  static void setFinalStatic(Field field, Object newValue) throws Exception {
    field.setAccessible(true);        
    Field modifiersField = Field.class.getDeclaredField("modifiers");
    modifiersField.setAccessible(true);
    modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
    field.set(null, newValue);
  }

  @Test
  public void getListTransactionFromTicketReturnIterableIterableTransactionListDto() throws Exception {
    Map<String, CompletableFuture<Iterable<Transaction>>> ticketMap = mock(Map.class);
    String ticket = "AAAA";
    Long transactionId = 1L;
    String description = "dummy";
    String walletName = "dummy";
    Long amount = 1L;
    Date date = mock(Date.class);
    TransactionType type = TransactionType.INCOME;
    CompletableFuture<Iterable<Transaction>> futureTransactionList = mock(CompletableFuture.class);
    List<Transaction> transactionList = new ArrayList<Transaction>();
    transactionList.add(transaction);

    lenient().when(ticketMap.remove(ticket))
        .thenReturn(futureTransactionList);
    when(futureTransactionList.get())
        .thenReturn(transactionList);
    lenient().when(transaction.getId())
        .thenReturn(transactionId);
    when(transaction.getWallet())
        .thenReturn(wallet);
    when(wallet.getName())
        .thenReturn(walletName);
    lenient().when(transaction.getDescription())
        .thenReturn(description);
    lenient().when(transaction.getAmount())
        .thenReturn(amount);
    when(transaction.getType())
        .thenReturn(type);
    when(transaction.getDate())
        .thenReturn(date);
    setFinalStatic(TransactionServiceImpl.class.getDeclaredField("ticketMap"), ticketMap);

    Iterable<TransactionListDto> returned = transactionService.getListTransactionFromTicket(ticket);

    for (TransactionListDto returnedDto : returned) {
      assertEquals(transactionId, returnedDto.getId());
      assertEquals(walletName, returnedDto.getWalletName());
      assertEquals(amount, returnedDto.getAmount());
      assertEquals(description, returnedDto.getDescription());
      assertEquals(type, returnedDto.getType());
      assertEquals(date, returnedDto.getDate());
    }
  }

  @Test
  public void getListTransactionFromTicketRaiseExceptionAndReturnIterableTransactionListDto() throws Exception {
    Map<String, CompletableFuture<Iterable<Transaction>>> ticketMap = mock(Map.class);
    String ticket = "AAAA";
    Long transactionId = 1L;
    String description = "dummy";
    String walletName = "dummy";
    Long amount = 1L;
    Date date = mock(Date.class);
    TransactionType type = TransactionType.INCOME;
    CompletableFuture<Iterable<Transaction>> futureTransactionList = mock(CompletableFuture.class);

    lenient().when(ticketMap.remove(ticket))
        .thenReturn(futureTransactionList);
    when(futureTransactionList.get())
        .thenReturn(null);
    setFinalStatic(TransactionServiceImpl.class.getDeclaredField("ticketMap"), ticketMap);

    Iterable<TransactionListDto> returned = transactionService.getListTransactionFromTicket(ticket);

    int cnt = 0;    
    for (TransactionListDto returnedDto : returned) {
      cnt++;
    }
    assertEquals(0, cnt);
  }
}