package com.c10.catatcash.transaction.service;

import com.c10.catatcash.transaction.dto.TransactionDto;
import com.c10.catatcash.transaction.dto.CreateTransactionValidatorDto;
import com.c10.catatcash.transaction.dto.DeleteTransactionValidatorDto;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.wallet.model.Wallet;
import com.c10.catatcash.wallet.repository.WalletRepository;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

@ExtendWith(MockitoExtension.class)
public class TransactionValidatorServiceImplTest {

  @Mock
  private WalletRepository walletRepository;

  @Mock
  private TransactionUtilityService transactionUtilityService;

  @InjectMocks
  private TransactionValidatorServiceImpl transactionValidatorService;

  private Wallet wallet;
  private TransactionDto transactionDto;
  private Transaction transaction;
  private Long walletId;
  private Optional<Wallet> optionalWallet;

  @BeforeEach
  public void setUp() {
    wallet = mock(Wallet.class);
    transactionDto = mock(TransactionDto.class);
    transaction = mock(Transaction.class);
    walletId = 1L;
    optionalWallet = Optional.ofNullable(wallet);
  }

  @Test
  public void whenAmountIsNegativeThenCreateTransactionIsNotValid() {
    when(transactionDto.getWallet())
        .thenReturn(wallet);
    when(wallet.getWalletId())
        .thenReturn(walletId);
    when(walletRepository.findById(walletId))
        .thenReturn(optionalWallet);
    when(transactionUtilityService.dtoToTransaction(transactionDto))
        .thenReturn(transaction);
    when(transactionUtilityService.isRequestMadeByWalletOwner(transaction))
        .thenReturn(transaction);
    when(transactionDto.getAmount())
        .thenReturn(-1L);

    CreateTransactionValidatorDto returned = transactionValidatorService.isValidCreateTransaction(transactionDto);
    assertFalse(returned.getIsValid());
  }

  @Test
  public void whenAmountIsZeroThenCreateTransactionIsNotValid() {
    when(transactionDto.getWallet())
        .thenReturn(wallet);
    when(wallet.getWalletId())
        .thenReturn(walletId);
    when(walletRepository.findById(walletId))
        .thenReturn(optionalWallet);
    when(transactionUtilityService.dtoToTransaction(transactionDto))
        .thenReturn(transaction);
    when(transactionUtilityService.isRequestMadeByWalletOwner(transaction))
        .thenReturn(transaction);
    when(transactionDto.getAmount())
        .thenReturn(0L);

    CreateTransactionValidatorDto returned = transactionValidatorService.isValidCreateTransaction(transactionDto);
    assertFalse(returned.getIsValid());
  }

  @Test
  public void whenUserDoesNotHaveTheWalletThatUsedInTransactionThenCreateTransactionIsNotValid() {
    when(transactionDto.getWallet())
        .thenReturn(wallet);
    when(wallet.getWalletId())
        .thenReturn(walletId);
    when(walletRepository.findById(walletId))
        .thenReturn(optionalWallet);
    when(transactionUtilityService.dtoToTransaction(transactionDto))
        .thenReturn(transaction);
    when(transactionUtilityService.isRequestMadeByWalletOwner(transaction))
        .thenReturn(null);

    CreateTransactionValidatorDto returned = transactionValidatorService.isValidCreateTransaction(transactionDto);
    assertFalse(returned.getIsValid());
  }

  @Test
  public void whenWalletIsNullThenCreateTransactionIsNotValid() {
    when(transactionDto.getWallet())
        .thenReturn(wallet);
    when(wallet.getWalletId())
        .thenReturn(walletId);
    when(walletRepository.findById(walletId))
        .thenReturn(Optional.ofNullable(null));
    when(transactionUtilityService.dtoToTransaction(transactionDto))
        .thenReturn(transaction);
    when(transactionUtilityService.isRequestMadeByWalletOwner(transaction))
        .thenReturn(transaction);

    CreateTransactionValidatorDto returned = transactionValidatorService.isValidCreateTransaction(transactionDto);
    assertFalse(returned.getIsValid());
  }

  @Test
  public void whenWalletBalanceIsLowerThanTransactionAmountThenCreateTransactionIsNotValid() {
    when(transactionDto.getWallet())
        .thenReturn(wallet);
    when(wallet.getWalletId())
        .thenReturn(walletId);
    when(walletRepository.findById(walletId))
        .thenReturn(optionalWallet);
    when(transactionUtilityService.dtoToTransaction(transactionDto))
        .thenReturn(transaction);
    when(transactionUtilityService.isRequestMadeByWalletOwner(transaction))
        .thenReturn(transaction);
    lenient().when(transactionDto.getAmount())
        .thenReturn(2L);
    lenient().when(transaction.getAmount())
        .thenReturn(-2L);
    lenient().when(wallet.getBalance())
        .thenReturn(1L);

    CreateTransactionValidatorDto returned = transactionValidatorService.isValidCreateTransaction(transactionDto);
    assertFalse(returned.getIsValid());
  }

  @Test
  public void whenEverythingSeemsCorrectThenCreateTransactionIsValid() {
    when(transactionDto.getWallet())
        .thenReturn(wallet);
    when(wallet.getWalletId())
        .thenReturn(walletId);
    when(walletRepository.findById(walletId))
        .thenReturn(optionalWallet);
    when(transactionUtilityService.dtoToTransaction(transactionDto))
        .thenReturn(transaction);
    when(transactionUtilityService.isRequestMadeByWalletOwner(transaction))
        .thenReturn(transaction);
    lenient().when(transactionDto.getAmount())
        .thenReturn(2L);
    lenient().when(transaction.getAmount())
        .thenReturn(2L);
    lenient().when(wallet.getBalance())
        .thenReturn(2L);

    CreateTransactionValidatorDto returned = transactionValidatorService.isValidCreateTransaction(transactionDto);
    assertTrue(returned.getIsValid());
  }

  @Test
  public void whenNotEveryDeletedTransactionIdIsValidIdThenDeleteTransactionIsNotValid() {
    List<Long> deletedTransactionIdList = new ArrayList<>();
    deletedTransactionIdList.add(1L);
    deletedTransactionIdList.add(2L);
    
    List<Transaction> deletedTransactionList = new ArrayList<>();
    deletedTransactionList.add(transaction);

    Map<Long, Long> balanceMap = new HashMap<>();
    balanceMap.put(1L, 1L);
    
    when(transactionUtilityService.getTransactionListByIdIn(deletedTransactionIdList))
        .thenReturn(deletedTransactionList);
    when(transactionUtilityService.getBalanceMap())
        .thenReturn(balanceMap);
    when(transaction.getWallet())
        .thenReturn(wallet);
    when(wallet.getWalletId())
        .thenReturn(1L);
    lenient().when(transaction.getAmount())
        .thenReturn(1L);

    DeleteTransactionValidatorDto returned = transactionValidatorService.isValidDeleteTransactionById(deletedTransactionIdList);
    assertFalse(returned.getIsValid());
  }

  @Test
  public void whenNotEveryDeletedTransactionAmountIsLowerOrEqualThanWalletBalanceThenDeleteTransactionIsNotValid() {
    List<Long> deletedTransactionIdList = new ArrayList<>();
    deletedTransactionIdList.add(1L);
    
    List<Transaction> deletedTransactionList = new ArrayList<>();
    deletedTransactionList.add(transaction);

    Map<Long, Long> balanceMap = new HashMap<>();
    balanceMap.put(1L, 1L);
    
    when(transactionUtilityService.getTransactionListByIdIn(deletedTransactionIdList))
        .thenReturn(deletedTransactionList);
    when(transactionUtilityService.getBalanceMap())
        .thenReturn(balanceMap);
    when(transaction.getWallet())
        .thenReturn(wallet);
    when(wallet.getWalletId())
        .thenReturn(1L);
    lenient().when(transaction.getAmount())
        .thenReturn(-5L);

    DeleteTransactionValidatorDto returned = transactionValidatorService.isValidDeleteTransactionById(deletedTransactionIdList);
    assertFalse(returned.getIsValid());
  }

  @Test
  public void whenNotEveryDeletedTransactionIsOwnedByUserThenDeleteTransactionIsNotValid() {
    List<Long> deletedTransactionIdList = new ArrayList<>();
    deletedTransactionIdList.add(1L);
    
    List<Transaction> deletedTransactionList = new ArrayList<>();
    deletedTransactionList.add(transaction);

    Map<Long, Long> balanceMap = new HashMap<>();
    balanceMap.put(10L, 1L);
    
    when(transactionUtilityService.getTransactionListByIdIn(deletedTransactionIdList))
        .thenReturn(deletedTransactionList);
    when(transactionUtilityService.getBalanceMap())
        .thenReturn(balanceMap);
    when(transaction.getWallet())
        .thenReturn(wallet);
    when(wallet.getWalletId())
        .thenReturn(walletId);
    lenient().when(transactionUtilityService.isEveryWalletBalanceGreaterOrEqualZero(balanceMap.values()))
        .thenReturn(true);
    lenient().when(transaction.getAmount())
        .thenReturn(1L);

    DeleteTransactionValidatorDto returned = transactionValidatorService.isValidDeleteTransactionById(deletedTransactionIdList);
    assertFalse(balanceMap.containsKey(walletId));
    assertFalse(returned.getIsValid());
  }

  @Test
  public void whenEverythingSeemsCorrectThenDeleteTransactionIsValid() {
    List<Long> deletedTransactionIdList = new ArrayList<>();
    deletedTransactionIdList.add(1L);
    
    List<Transaction> deletedTransactionList = new ArrayList<>();
    deletedTransactionList.add(transaction);

    Map<Long, Long> balanceMap = new HashMap<>();
    balanceMap.put(1L, 1L);
    
    when(transactionUtilityService.getTransactionListByIdIn(deletedTransactionIdList))
        .thenReturn(deletedTransactionList);
    when(transactionUtilityService.getBalanceMap())
        .thenReturn(balanceMap);
    when(transaction.getWallet())
        .thenReturn(wallet);
    when(wallet.getWalletId())
        .thenReturn(1L);
    lenient().when(transaction.getAmount())
        .thenReturn(1L);
    lenient().when(transactionUtilityService.isEveryWalletBalanceGreaterOrEqualZero(any(Collection.class)))
        .thenReturn(true);

    DeleteTransactionValidatorDto returned = transactionValidatorService.isValidDeleteTransactionById(deletedTransactionIdList);
    assertTrue(returned.getIsValid());
  }
}