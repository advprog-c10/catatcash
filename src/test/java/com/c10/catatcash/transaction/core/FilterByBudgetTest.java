package com.c10.catatcash.transaction.core;

import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.repository.TransactionRepository;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.wallet.model.Wallet;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class FilterByBudgetTest {

  @Mock
  private TransactionRepository transactionRepository;

  private Class<?> filterByBudgetClass;

  @BeforeEach
  public void setUp() throws Exception {
    filterByBudgetClass = Class.forName("com.c10.catatcash.transaction.core.FilterByBudget");
  }

  @Test
  public void testFilterByBudgetIsAFilterStrategy() {
    Collection<Type> interfaces = Arrays.asList(filterByBudgetClass.getInterfaces());

    assertTrue(interfaces.stream()
        .anyMatch(type -> type.getTypeName()
        .equals("com.c10.catatcash.transaction.core.FilterStrategy")));
    }

  @Test
  public void testFilterByBudgetOverrideGetTypeMethod() throws Exception {
    Method getType = filterByBudgetClass.getDeclaredMethod("getType");

    assertTrue(Modifier.isPublic(getType.getModifiers()));
    assertEquals("java.lang.String", getType.getGenericReturnType().getTypeName());
  }

  @Test
  public void testFilterByBudgetOverrideGetFilteredTransaction() throws Exception {
    Class[] cArg = new Class[2];
    cArg[0] = Class.forName("java.lang.String");
    cArg[1] = Class.forName("com.c10.catatcash.userauth.model.User");
    Method getFilteredTransaction = filterByBudgetClass
        .getDeclaredMethod("getFilteredTransaction", cArg);

    assertTrue(Modifier.isPublic(getFilteredTransaction.getModifiers()));
    assertEquals("java.lang.Iterable<com.c10.catatcash.transaction.model.Transaction>", getFilteredTransaction.getGenericReturnType().getTypeName());
  }

  @Test
  public void testGetFilteredTransactionCallTransactionRepositoryAndReturnCorrectValue() {
    User mockUser = new User();
    Iterable<Transaction> mockTransactionIterable = new ArrayList<>();
    String mockParam = "1";

    when(transactionRepository
          .findByBudgetIdAndWalletAppUserOrderByDateAsc(Long.parseLong(mockParam), mockUser))
        .thenReturn(mockTransactionIterable);

    FilterByBudget filter = new FilterByBudget(transactionRepository);
    Iterable<Transaction> callGetFiltered = filter.getFilteredTransaction(mockParam, mockUser);

    assertEquals(mockTransactionIterable, callGetFiltered);

    verify(transactionRepository, times(1))
        .findByBudgetIdAndWalletAppUserOrderByDateAsc(Long.parseLong(mockParam), mockUser);
  }

  @Test
  public void testGetFilteredTransactionReturnNull() {
    User mockUser = new User();
    FilterByBudget filter = new FilterByBudget(transactionRepository);
    String mockParam = "A";

    Iterable<Transaction> callGetFiltered = filter.getFilteredTransaction(mockParam, mockUser);
    assertEquals(null, callGetFiltered);
  }
}