package com.c10.catatcash.transaction.core;

import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.repository.TransactionRepository;
import com.c10.catatcash.userauth.model.User;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class FilterByNoFilterTest {

  @Mock
  private TransactionRepository transactionRepository;

  private Class<?> filterByNoFilterClass;

  @BeforeEach
  public void setUp() throws Exception {
    filterByNoFilterClass = Class.forName("com.c10.catatcash.transaction.core.FilterByNoFilter");
  }

  @Test
  public void testFilterByNoFilterIsAFilterStrategy() {
    Collection<Type> interfaces = Arrays.asList(filterByNoFilterClass.getInterfaces());

    assertTrue(interfaces.stream()
        .anyMatch(type -> type.getTypeName()
        .equals("com.c10.catatcash.transaction.core.FilterStrategy")));
    }

  @Test
  public void testFilterByNoFilterOverrideGetTypeMethod() throws Exception {
    Method getType = filterByNoFilterClass.getDeclaredMethod("getType");

    assertTrue(Modifier.isPublic(getType.getModifiers()));
    assertEquals("java.lang.String", getType.getGenericReturnType().getTypeName());
  }

  @Test
  public void testFilterByNoFilterOverrideGetFilteredTransaction() throws Exception {
    Class[] cArg = new Class[2];
    cArg[0] = Class.forName("java.lang.String");
    cArg[1] = Class.forName("com.c10.catatcash.userauth.model.User");
    Method getFilteredTransaction = filterByNoFilterClass
        .getDeclaredMethod("getFilteredTransaction", cArg);

    assertTrue(Modifier.isPublic(getFilteredTransaction.getModifiers()));
    assertEquals("java.lang.Iterable<com.c10.catatcash.transaction.model.Transaction>", getFilteredTransaction.getGenericReturnType().getTypeName());
  }

  @Test
  public void testGetFilteredTransactionCallTransactionRepositoryAndReturnCorrectValue() {
    User mockUser = new User();
    Iterable<Transaction> mockTransactionIterable = new ArrayList<>();
    String mockParam = "";
    
    when(transactionRepository.findByWalletAppUserOrderByDateAsc(mockUser))
        .thenReturn(mockTransactionIterable);

    FilterByNoFilter filter = new FilterByNoFilter(transactionRepository);
    Iterable<Transaction> callGetFiltered = filter.getFilteredTransaction(mockParam, mockUser);

    assertEquals(mockTransactionIterable, callGetFiltered);

    verify(transactionRepository, times(1))
        .findByWalletAppUserOrderByDateAsc(mockUser);
  }
}