package com.c10.catatcash.transaction.core;

import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.repository.TransactionRepository;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.wallet.model.Wallet;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class FilterByTimestampIntervalTest {

  @Mock
  private TransactionRepository transactionRepository;

  private Class<?> filterByTimestampIntervalClass;

  @BeforeEach
  public void setUp() throws Exception {
    filterByTimestampIntervalClass = Class.forName("com.c10.catatcash.transaction.core.FilterByTimestampInterval");
  }

  @Test
  public void testFilterByTimestampIntervalIsAFilterStrategy() {
    Collection<Type> interfaces = Arrays.asList(filterByTimestampIntervalClass.getInterfaces());

    assertTrue(interfaces.stream()
        .anyMatch(type -> type.getTypeName()
        .equals("com.c10.catatcash.transaction.core.FilterStrategy")));
    }

  @Test
  public void testFilterByTimestampIntervalOverrideGetTypeMethod() throws Exception {
    Method getType = filterByTimestampIntervalClass.getDeclaredMethod("getType");

    assertTrue(Modifier.isPublic(getType.getModifiers()));
    assertEquals("java.lang.String", getType.getGenericReturnType().getTypeName());
  }

  @Test
  public void testFilterByTimestampIntervalOverrideGetFilteredTransaction() throws Exception {
    Class[] cArg = new Class[2];
    cArg[0] = Class.forName("java.lang.String");
    cArg[1] = Class.forName("com.c10.catatcash.userauth.model.User");
    Method getFilteredTransaction = filterByTimestampIntervalClass
        .getDeclaredMethod("getFilteredTransaction", cArg);

    assertTrue(Modifier.isPublic(getFilteredTransaction.getModifiers()));
    assertEquals("java.lang.Iterable<com.c10.catatcash.transaction.model.Transaction>", getFilteredTransaction.getGenericReturnType().getTypeName());
  }

  @Test
  public void testGetFilteredTransactionCallTransactionRepositoryAndReturnCorrectValue() throws Exception {
    User mockUser = new User();
    Iterable<Transaction> mockTransactionIterable = new ArrayList<>();
    String mockParam = "2021-01-01,2021-01-01";
    
    Date mockDateStart = new SimpleDateFormat("yyyy-MM-dd").parse("2021-01-01");
    Date mockDateEnd = new SimpleDateFormat("yyyy-MM-dd").parse("2021-01-01");

    when(transactionRepository
          .findByDateGreaterThanEqualAndDateLessThanEqualAndWalletAppUserOrderByDateAsc(mockDateStart, mockDateEnd, mockUser))
        .thenReturn(mockTransactionIterable);

    FilterByTimestampInterval filter = new FilterByTimestampInterval(transactionRepository);
    Iterable<Transaction> callGetFiltered = filter.getFilteredTransaction(mockParam, mockUser);

    assertEquals(mockTransactionIterable, callGetFiltered);

    verify(transactionRepository, times(1))
        .findByDateGreaterThanEqualAndDateLessThanEqualAndWalletAppUserOrderByDateAsc(mockDateStart, mockDateEnd, mockUser);
  }

  @Test
  public void testGetFilteredTransactionReturnNullBecauseArrayOutOfBoundsException() {
    User mockUser = new User();
    FilterByTimestampInterval filter = new FilterByTimestampInterval(transactionRepository);
    String mockParam = "2021-01-01";

    Iterable<Transaction> callGetFiltered = filter.getFilteredTransaction(mockParam, mockUser);
    assertEquals(null, callGetFiltered);
  }

  @Test
  public void testGetFilteredTransactionReturnNullBecauseParseException() {
    User mockUser = new User();
    FilterByTimestampInterval filter = new FilterByTimestampInterval(transactionRepository);
    String mockParam = "2021/01/01,2021/01/01";

    Iterable<Transaction> callGetFiltered = filter.getFilteredTransaction(mockParam, mockUser);
    assertEquals(null, callGetFiltered);
  }
}