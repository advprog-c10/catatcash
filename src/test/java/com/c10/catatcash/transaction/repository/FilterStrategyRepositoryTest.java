package com.c10.catatcash.transaction.repository;

import com.c10.catatcash.transaction.core.FilterStrategy;
import com.c10.catatcash.transaction.core.FilterByNoFilter;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.repository.TransactionRepository;
import com.c10.catatcash.userauth.model.User;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class FilterStrategyRepositoryTest {
  
  @Mock
  private TransactionRepository transactionRepository;

  @InjectMocks
  private FilterStrategyRepository filterStrategyRepository;

  private Class<?> filterStrategyRepositoryClass;
  private User mockUser;
  private Iterable<Transaction> mockTransactionIterable;

  @BeforeEach
  public void setUp() throws Exception {
    filterStrategyRepositoryClass = Class.forName("com.c10.catatcash.transaction.repository.FilterStrategyRepository");
    mockUser = new User();
    mockTransactionIterable = new ArrayList<>();
  }

  @Test
  public void getFilteredTransactionExists() throws Exception {
    Class[] cArg = new Class[3];
    cArg[0] = Class.forName("java.lang.String");
    cArg[1] = Class.forName("java.lang.String");
    cArg[2] = Class.forName("com.c10.catatcash.userauth.model.User");

    Method getFilteredTransaction = filterStrategyRepositoryClass
        .getDeclaredMethod("getFilteredTransaction", cArg);
    
    assertTrue(Modifier.isPublic(getFilteredTransaction.getModifiers()));
    assertEquals("java.lang.Iterable<com.c10.catatcash.transaction.model.Transaction>", getFilteredTransaction.getGenericReturnType().getTypeName());
  }

  @Test
  public void getFilterStrategyByTypeExists() throws Exception {
    Class[] cArg = new Class[1];
    cArg[0] = Class.forName("java.lang.String");

    Method getFilterStrategyByType = filterStrategyRepositoryClass
        .getDeclaredMethod("getFilterStrategyByType", cArg);
    
    assertTrue(Modifier.isPublic(getFilterStrategyByType.getModifiers()));
    assertEquals("com.c10.catatcash.transaction.core.FilterStrategy", getFilterStrategyByType.getGenericReturnType().getTypeName());
  }

  @Test
  public void addFilterStrategyExists() throws Exception {
    Class[] cArg = new Class[1];
    cArg[0] = Class.forName("com.c10.catatcash.transaction.core.FilterStrategy");

    Method addFilterStrategy = filterStrategyRepositoryClass
        .getDeclaredMethod("addFilterStrategy", cArg);
    
    assertTrue(Modifier.isPublic(addFilterStrategy.getModifiers()));
    assertEquals("void", addFilterStrategy.getGenericReturnType().getTypeName());
  }

  @Test
  public void getFilterStrategyByTypeReturnCorrectStrategy() {
    FilterStrategy mockFilterStrategy = new FilterByNoFilter(transactionRepository);
    filterStrategyRepository.addFilterStrategy(mockFilterStrategy);
    FilterStrategy returned = filterStrategyRepository.getFilterStrategyByType(mockFilterStrategy.getType());
    assertEquals(mockFilterStrategy, returned);
  }

  @Test
  public void getFilterStrategyByTypeReturnNull() {
    FilterStrategy returned = filterStrategyRepository.getFilterStrategyByType("dummy");
    assertEquals(null, returned);
  }

  @Test
  public void getFilteredTransactionReturnCorrectValue() {
    FilterStrategy mockFilterStrategy = new FilterByNoFilter(transactionRepository);
    filterStrategyRepository.addFilterStrategy(mockFilterStrategy);
    String mockFilterType = mockFilterStrategy.getType();
    String mockFilterParam = "dummy";
    
    when(mockFilterStrategy.getFilteredTransaction(mockFilterParam, mockUser))
        .thenReturn(mockTransactionIterable);
    
    Iterable<Transaction> callGetFiltered = filterStrategyRepository.getFilteredTransaction(mockFilterType, mockFilterParam, mockUser);
    
    assertEquals(mockTransactionIterable, callGetFiltered);
  }

  @Test
  public void getFilteredTransactionReturnNull() {
    String mockFilterType = "dummy";
    String mockFilterParam = "dummy";

    Iterable<Transaction> callGetFiltered = filterStrategyRepository.getFilteredTransaction(mockFilterType, mockFilterParam, mockUser);
    
    assertEquals(null, callGetFiltered);
  }
}