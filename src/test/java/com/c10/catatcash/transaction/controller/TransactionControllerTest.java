package com.c10.catatcash.transaction.controller;

import com.c10.catatcash.budget.service.BudgetService;
import com.c10.catatcash.transaction.dto.CreateTransactionValidatorDto;
import com.c10.catatcash.transaction.dto.DeleteTransactionValidatorDto;
import com.c10.catatcash.transaction.dto.TransactionDto;
import com.c10.catatcash.transaction.dto.TransactionListDto;
import com.c10.catatcash.transaction.model.Transaction;
import com.c10.catatcash.transaction.service.TransactionService;
import com.c10.catatcash.transaction.service.TransactionValidatorService;
import com.c10.catatcash.userauth.service.UserService;
import com.c10.catatcash.wallet.service.WalletService;

import java.util.ArrayList;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Test;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.mockito.Mockito.*;

@WebMvcTest(controllers = TransactionController.class)
@ExtendWith(MockitoExtension.class)
public class TransactionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TransactionService transactionService;

    @MockBean
    private WalletService walletService;

    @MockBean
    private TransactionValidatorService transactionValidatorService;

    @MockBean
    private UserService userService;

    @MockBean
    private BudgetService budgetService;

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    public void testTransactionHomeBeOk() throws Exception {
      when(transactionService.getTicketForGetListTransactionByFilter("by-nofilter", ""))
          .thenReturn("AAA");

      this.mvc.perform(get("/transaction"))
          .andExpect(status().isOk())
          .andExpect(handler().methodName("transactionHome"))
          .andExpect(model().attributeExists("transactionListTicket"))
          .andExpect(model().attributeExists("walletList"))
          .andExpect(model().attributeExists("budgetList"))
          .andExpect(view().name("transaction/transaction"));
      verify(transactionService, times(1)).getTicketForGetListTransactionByFilter("by-nofilter", "");
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    public void testTransactionHomeBeOkAndGetTransactionListTicketNotFromServiceWhenIsFilteredNotNull() throws Exception {
      this.mvc.perform(get("/transaction")
              .flashAttr("isFiltered", true)
              .flashAttr("transactionListTicket", "AAA"))
          .andExpect(status().isOk())
          .andExpect(handler().methodName("transactionHome"))
          .andExpect(model().attributeExists("transactionListTicket"))
          .andExpect(model().attributeExists("walletList"))
          .andExpect(view().name("transaction/transaction"));
      verify(transactionService, times(0)).getTicketForGetListTransactionByFilter("by-nofilter", "");
    }


    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    public void testGetRequestCreateTransactionReturnTransactionDto() throws Exception {
      this.mvc.perform(get("/transaction/create"))
          .andExpect(handler().methodName("createTransaction"))
          .andExpect(model().attributeExists("wallets"))
          .andExpect(model().attributeExists("newTransaction"));
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    public void testGetTransactionListFromTicketReturnTransactionList() throws Exception {
      String ticket = "AAA";
      Iterable<TransactionListDto> iterableMock = new ArrayList<>();
      when(transactionService.getListTransactionFromTicket(ticket))
          .thenReturn(iterableMock);
      this.mvc.perform(post("/transaction/getTransactionListFromTicket")
              .param("ticket", ticket)
              .with(csrf())
          ).andExpect(handler().methodName("getTransactionListFromTicket"))
          .andExpect(status().isOk())
          .andExpect(content().contentType("application/json"));
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    public void testChangeFilterRedirectToTransactionHome() throws Exception {
      when(transactionService.getTicketForGetListTransactionByFilter("by-nofilter", ""))
          .thenReturn("AAA");
      String filterType = "by-nofilter";
      String filterParam = "";
      this.mvc.perform(get("/transaction/filter")
              .param("filterType", filterType)
              .param("filterParam", filterParam))
          .andExpect(redirectedUrl("/transaction"))
          .andExpect(handler().methodName("changeFilter"))
          .andExpect(flash().attributeExists("isFiltered"))
          .andExpect(flash().attributeExists("transactionListTicket"));
      verify(transactionService, times(1)).getTicketForGetListTransactionByFilter(filterType, filterParam);
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    public void testValidPostRequestCreateTransactionAndRedirectToHome() throws Exception {
      TransactionDto newTransactionDto = mock(TransactionDto.class);
      Transaction newTransaction = mock(Transaction.class);
      CreateTransactionValidatorDto validator = mock(CreateTransactionValidatorDto.class);
      Boolean isValid = true;
      
      when(transactionValidatorService.isValidCreateTransaction(any()))
          .thenReturn(validator);
      when(validator.getIsValid())
          .thenReturn(isValid);
      when(validator.getTransaction())
          .thenReturn(newTransaction);

      this.mvc.perform(post("/transaction/create")
              .flashAttr("newTransactionDto", newTransactionDto)
              .with(csrf()))
          .andExpect(redirectedUrl("/transaction"))
          .andExpect(handler().methodName("createTransaction"));

      verify(transactionService, times(1)).createTransaction(newTransaction);
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    public void testInvalidPostRequestRejectCreateTransactionAndRedirectToFailPage() throws Exception {
      TransactionDto newTransactionDto = mock(TransactionDto.class);
      CreateTransactionValidatorDto validator = mock(CreateTransactionValidatorDto.class);
      Boolean isValid = false;
      
      when(transactionValidatorService.isValidCreateTransaction(any()))
          .thenReturn(validator);
      when(validator.getIsValid())
          .thenReturn(isValid);

      this.mvc.perform(post("/transaction/create")
              .flashAttr("newTransactionDto", newTransactionDto)
              .with(csrf()))
          .andExpect(handler().methodName("createTransaction"))
          .andExpect(view().name("transaction/fail_operation"));

      verify(transactionService, times(0)).createTransaction(any());
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    public void testDeleteTransactionGetANonNumberInTransactionIdListReturnFailPage() throws Exception {
      String deletedTransactions = "1,A,B";

      this.mvc.perform(post("/transaction/delete")
              .param("deletedTransactions", deletedTransactions)
              .with(csrf()))
          .andExpect(handler().methodName("deleteTransactions"))
          .andExpect(view().name("transaction/fail_operation"));
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    public void testDeleteTransactionGetNullParameterThenRedirectToTransactionHome() throws Exception {
      String deletedTransactions = null;
      this.mvc.perform(post("/transaction/delete")
              .param("deletedTransactions", deletedTransactions)
              .with(csrf()))
          .andExpect(handler().methodName("deleteTransactions"))
          .andExpect(redirectedUrl("/transaction"));
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    public void testDeleteTransactionGetInvalidRequestReturnFailPage() throws Exception {
      String deletedTransactions = "1,2";
      DeleteTransactionValidatorDto validator = mock(DeleteTransactionValidatorDto.class);
      Boolean isValid = false;

      when(transactionValidatorService.isValidDeleteTransactionById(any()))
          .thenReturn(validator);
      when(validator.getIsValid())
          .thenReturn(isValid);

      this.mvc.perform(post("/transaction/delete")
              .param("deletedTransactions", deletedTransactions)
              .with(csrf()))
          .andExpect(handler().methodName("deleteTransactions"))
          .andExpect(view().name("transaction/fail_operation"));
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    public void testDeleteTransactionGetValidRequestRedirectToTransactionHomeAndDeleteTransaction() throws Exception {
      String deletedTransactions = "1,2";
      DeleteTransactionValidatorDto validator = mock(DeleteTransactionValidatorDto.class);
      Boolean isValid = true;

      when(transactionValidatorService.isValidDeleteTransactionById(any()))
          .thenReturn(validator);
      when(validator.getIsValid())
          .thenReturn(isValid);

      this.mvc.perform(post("/transaction/delete")
              .param("deletedTransactions", deletedTransactions)
              .with(csrf()))
          .andExpect(handler().methodName("deleteTransactions"))
          .andExpect(redirectedUrl("/transaction"));

      verify(transactionService, times(1)).deleteTransactions(any());
    }

  @Test
  @WithMockUser(username = "user1", password = "pwd", roles = "USER")
  public void validateCreateTransactionReturnValidationStatus() throws Exception {
    TransactionDto newTransactionDto = mock(TransactionDto.class);
    CreateTransactionValidatorDto validator = mock(CreateTransactionValidatorDto.class);
    when(transactionValidatorService.isValidCreateTransaction(any()))
        .thenReturn(validator);
    when(validator.getIsValid())
        .thenReturn(true);
    
    this.mvc.perform(post("/transaction/validate/create")
            .flashAttr("newTransactionDto", newTransactionDto)
            .with(csrf())
        ).andExpect(status().isOk())
        .andExpect(handler().methodName("validateCreateTransaction"))
        .andExpect(content().contentType("application/json"));
  }
}
