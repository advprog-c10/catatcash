package com.c10.catatcash.userauth.service;

import com.c10.catatcash.userauth.dto.UserRegistrationDto;
import com.c10.catatcash.userauth.model.Role;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.userauth.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    @Mock
    private WebClient webClient;

    @Mock
    WebClient.RequestHeadersSpec requestHeadersSpec;

    @Mock
    WebClient.RequestBodyUriSpec requestBodyUriSpec;

    @Mock
    WebClient.RequestBodySpec requestBodySpec;

    @Mock
    WebClient.ResponseSpec responseSpec;

    @InjectMocks
    private UserServiceImpl userService;

    private User user;

    @BeforeEach
    void setUp() {
        user = new User(
                "advprogc10@gmail.com",
                "4ea1be751a72",
                Collections.singletonList(new Role("ROLE_USER"))
        );
    }

    @Test
    void testServiceFindUserByEmail() {
        when(userRepository.findByEmail(any(String.class))).thenReturn(user);
        userService.findByEmail("advprogc10@gmail.com");
        verify(userRepository, atLeastOnce()).findByEmail(any(String.class));
    }

    @Test
    void testServiceSaveUser() {
        UserRegistrationDto registrationDto = new UserRegistrationDto();
        registrationDto.setEmail("advprogc10@gmail.com");
        registrationDto.setPassword("4ea1be751a72");

        when(passwordEncoder.encode(any(String.class))).thenReturn(user.getPassword());
        when(userRepository.save(any(User.class))).thenReturn(user);
        User result = userService.save(registrationDto);
        verify(userRepository, atLeastOnce()).save(any(User.class));
        assertEquals(result.getEmail(), user.getEmail());
    }

    @Test
    void testServiceLoadUserByUsernameUserFound() {
        when(userRepository.findByEmail(any(String.class))).thenReturn(user);

        UserDetails result = userService.loadUserByUsername("advprogc10@gmail.com");
        verify(userRepository, atLeastOnce()).findByEmail(any(String.class));
        assertEquals(result.getUsername(), user.getEmail());
    }

    @Test
    void testServiceLoadUserByUsernameUserNotFound() {
        when(userRepository.findByEmail(any(String.class))).thenReturn(null);

        Exception exception = assertThrows(UsernameNotFoundException.class, () ->
                userService.loadUserByUsername("dummy@email.com"));

        assertTrue(exception.getMessage().contains("Invalid username or password."));
    }

    @Test
    void testServiceSendEmailSuccess() {
        UserRegistrationDto registrationDto = new UserRegistrationDto();
        registrationDto.setEmail("advprogc10@gmail.com");

        when(webClient.post()).thenReturn(requestBodyUriSpec);
        when(requestBodyUriSpec.uri(anyString())).thenReturn(requestBodySpec);
        lenient().when(requestBodySpec.header(any(),any())).thenReturn(requestBodySpec);
        when(requestBodySpec.body(any(Mono.class), (Class<?>) any())).thenReturn(requestHeadersSpec);
        when(requestHeadersSpec.retrieve()).thenReturn(responseSpec);
        when(responseSpec.bodyToMono(ArgumentMatchers.<Class<UserRegistrationDto>>notNull()))
                .thenReturn(Mono.just(registrationDto));

        userService.sendEmail(registrationDto);

        verify(webClient, atLeastOnce()).post();
        assertTrue(true);
    }
}
