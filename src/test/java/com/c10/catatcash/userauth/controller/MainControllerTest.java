package com.c10.catatcash.userauth.controller;

import com.c10.catatcash.userauth.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = MainController.class)
@ExtendWith(MockitoExtension.class)
class MainControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userService;

    @Test
    void testGetHomePageNotLoggedInStatusShouldBeOk() throws Exception {
        this.mvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void testGetHomePageLoggedInStatusShouldBeOk() throws Exception {
        this.mvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isFound());
    }

    @Test
    void testGetLoginPageNotLoggedInStatusShouldBeOk() throws Exception {
        this.mvc.perform(get("/login"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "user1", password = "pwd", roles = "USER")
    void testGetLoginPageLoggedInStatusShouldBeOk() throws Exception {
        this.mvc.perform(get("/login"))
                .andDo(print())
                .andExpect(status().isFound());
    }
}
