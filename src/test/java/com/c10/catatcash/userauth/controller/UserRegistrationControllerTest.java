package com.c10.catatcash.userauth.controller;

import com.c10.catatcash.userauth.dto.UserRegistrationDto;
import com.c10.catatcash.userauth.model.Role;
import com.c10.catatcash.userauth.model.User;
import com.c10.catatcash.userauth.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = UserRegistrationController.class)
@ExtendWith(MockitoExtension.class)
class UserRegistrationControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userService;

    private User user;

    @BeforeEach
    void init() {
        user = new User(
                "test@email.com",
                "4ea1be751a72",
                Collections.singletonList(new Role("ROLE_USER"))
        );
    }

    @Test
    @WithAnonymousUser
    void testGetRegisterPageShouldBeOk() throws Exception {
        this.mvc.perform(get("/register"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithAnonymousUser
    void testPostRegisterPageSuccessfulRegistration() throws Exception {
        UserRegistrationDto registrationDto = new UserRegistrationDto();
        registrationDto.setEmail("dummy@email.com");
        registrationDto.setPassword("27a157eb1ae4");

        this.mvc.perform(post("/register")
                .param("email", registrationDto.getEmail())
                .param("password", registrationDto.getPassword())
                .with(csrf()))
                .andExpect(status().isFound());
        verify(userService, times(1)).save(registrationDto);
    }

    @Test
    @WithAnonymousUser
    void testPostRegisterPageFailedExistingEmail() throws Exception {
        UserRegistrationDto registrationDto = new UserRegistrationDto();
        registrationDto.setEmail("test@email.com");
        registrationDto.setPassword("4ea1be751a72");

        when(userService.findByEmail(registrationDto.getEmail())).thenReturn(user);

        this.mvc.perform(post("/register")
                .param("email", registrationDto.getEmail())
                .param("password", registrationDto.getPassword())
                .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(model().attribute("errorMessage", "Oops!  There is already a user registered with the email provided."))
                .andExpect(view().name("userauth/registration"));
    }

    @Test
    @WithAnonymousUser
    void testPostRegisterPageFailedWeakPassword() throws Exception {
        UserRegistrationDto registrationDto = new UserRegistrationDto();
        registrationDto.setEmail("dummy@email.com");
        registrationDto.setPassword("password");

        this.mvc.perform(post("/register")
                .param("email", registrationDto.getEmail())
                .param("password", registrationDto.getPassword())
                .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(model().attribute("errorMessage", "Your password is too weak. This is a top-10 common password."))
                .andExpect(view().name("userauth/registration"));
    }
}
