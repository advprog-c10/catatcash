# CatatCash
## Final Project Advanced Programming 2020/2021 of Group C10

[![pipeline status](https://gitlab.com/advprog-c10/catatcash/badges/master/pipeline.svg)](https://gitlab.com/advprog-c10/catatcash/-/commits/master)
[![coverage report](https://gitlab.com/advprog-c10/catatcash/badges/master/coverage.svg)](https://gitlab.com/advprog-c10/catatcash/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=advprog-c10_catatcash&metric=alert_status)](https://sonarcloud.io/dashboard?id=advprog-c10_catatcash)


| Name                   | Student ID | Feature             |
|------------------------|------------|---------------------|
| Palito                 | 1706039793 | Report              |
| Prajna                 | 1906293285 | Transaction         |
| Muhammad Irfan Junaidi | 1906293202 | User Authentication |
| Muhammad Hanif Anggawi | 1906350963 | Wallet              |
| Aimar Fikri Salafi     | 1906400186 | Budgeting           |
